

var country_arr = new Array( "Australia","India", "New Zealand", "Pakistan","South Africa");


var s_a = new Array();

s_a[1]="Australian Capital Territory|New South Wales|Northern Territory|Queensland|South Australia|Tasmania|Victoria|Western Australia";
s_a[2]="Andhra Pradesh|Assam|Bihar|Daman and Diu|Delhi|Goa|Gujarat|Haryana|Himachal Pradesh|Jammu and Kashmir|Jharkhand|Karnataka|Kerala|Madhya Pradesh|Maharashtra|Punjab|Rajasthan|Tamil Nadu|Uttar Pradesh";
s_a[3]="Akaroa|Amuri|Ashburton|Bay of Islands|Bruce|Buller|Chatham Islands|Cheviot|Clifton|Clutha|Cook";
s_a[4]="Balochistan|Federally Administered Tribal Areas|Islamabad Capital Territory|North-West Frontier Province|Punjab|Sindh";
s_a[5]="Eastern Cape|Free State|Gauteng|KwaZulu-Natal|Mpumalanga|North-West|Northern Cape|Northern Province|Western Cape";


var s_c = new Array();

s_c[1]="Australian Capital Territory|New South Wales|Northern Territory|Queensland|South Australia|Tasmania|Victoria|Western Australia";
s_c[2]="Andhra Pradesh|Assam|Bihar|Daman and Diu|Delhi|Goa|Gujarat|Haryana|Himachal Pradesh|Jammu and Kashmir|Jharkhand|Karnataka|Kerala|Madhya Pradesh|Maharashtra|Punjab|Rajasthan|Tamil Nadu|Uttar Pradesh";
s_c[3]="Akaroa|Amuri|Ashburton|Bay of Islands|Bruce|Buller|Chatham Islands|Cheviot|Clifton|Clutha|Cook";
s_c[4]="Balochistan|Federally Administered Tribal Areas|Islamabad Capital Territory|North-West Frontier Province|Punjab|Sindh";
s_c[5]="Eastern Cape|Free State|Gauteng|KwaZulu-Natal|Mpumalanga|North-West|Northern Cape|Northern Province|Western Cape";
s_c[7]="Ahmedabad|Vadodara|Anand|Chhota Udaipur|Dahod|Kheda|Mahisagar|Panchmahal|Gandhinagar|Aravalli|Banaskantha|Mehsana|Patan|Sabarkantha|Rajkot|Amreli|Bhavnagar|Botad|Devbhoomi Dwarka|Gir Somnath|Jamnagar|Junagadh|Morbi|Porbandar|Surendranagar|Kachchh|Surat|Bharuch|Dang|Narmada|Navsari|Tapi|Valsad";


function populateStates( countryElementId, stateElementId ,cityElementId){
	
	var selectedCountryIndex = document.getElementById( countryElementId ).selectedIndex;

	var stateElement = document.getElementById( stateElementId );
	
	stateElement.length=0;	// Fixed by Julian Woods
	stateElement.options[0] = new Option('Select State','-1');
	stateElement.selectedIndex = 0;
	
	var state_arr = s_a[selectedCountryIndex].split("|");
	
	for (var i=0; i<state_arr.length; i++) {
		stateElement.options[stateElement.length] = new Option(state_arr[i],state_arr[i]);
	}
	
	if( cityElementId ){
	 stateElement.onchange = function(){
			populateCities(stateElementId,cityElementId );
		};
	}
}

function populateCountries(countryElementId, stateElementId,cityElementId){
	// given the id of the <select> tag as function argument, it inserts <option> tags
	var countryElement = document.getElementById(countryElementId);
	countryElement.length=0;
	countryElement.options[0] = new Option('Select Country','-1');
	countryElement.selectedIndex = 0;
	for (var i=0; i<country_arr.length; i++) {
		countryElement.options[countryElement.length] = new Option(country_arr[i],country_arr[i]);
	}

	// Assigned all countries. Now assign event listener for the states.
	
	if( stateElementId ){
		countryElement.onchange = function(){
			populateStates( countryElementId, stateElementId ,cityElementId);
		};
	}
}

function populateCities(stateElementId, cityElementId){
	
	var selectedStateIndex = document.getElementById( stateElementId ).selectedIndex;

	var cityElement = document.getElementById( cityElementId );
	
	cityElement.length=0;	// Fixed by Julian Woods
	cityElement.options[0] = new Option('Select State','-1');
	cityElement.selectedIndex = 0;
	
	var city_arr = s_c[selectedStateIndex].split("|");
	
	for (var i=0; i<city_arr.length; i++) {
		cityElement.options[cityElement.length] = new Option(city_arr[i],city_arr[i]);
	}
}