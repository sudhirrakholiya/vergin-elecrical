package com.bonrix.vergin.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="categorymaster",uniqueConstraints={@UniqueConstraint(columnNames={"catName"})})
public class CategoryMaster {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "catId")
    private long catId;
    
    @Column(name = "catName")
    private String catName;
    
    @Column(name = "docName")
    private String docName;
    
    @Column(name = "parentCatId")
    private long parentCatId;

	public long getCatId() {
		return catId;
	}

	public void setCatId(long catId) {
		this.catId = catId;
	}

	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public long getParentCatId() {
		return parentCatId;
	}

	public void setParentCatId(long parentCatId) {
		this.parentCatId = parentCatId;
	}

}
