package com.bonrix.vergin.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="smssetting")
public class SMSSetting {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "setId")
    private long setId;
    
    @Column(name = "apiURL")
    private String apiURL;
    
    @Column(name = "succKey")
    private String succKey;
    
    @Column(name = "provider")
    private String provider;
    
    @Column(name = "settStatus")
    private boolean settStatus;
    
	public long getSetId() {
		return setId;
	}

	public void setSetId(long setId) {
		this.setId = setId;
	}

	public String getApiURL() {
		return apiURL;
	}

	public void setApiURL(String apiURL) {
		this.apiURL = apiURL;
	}

	public String getSuccKey() {
		return succKey;
	}

	public void setSuccKey(String succKey) {
		this.succKey = succKey;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public boolean isSettStatus() {
		return settStatus;
	}

	public void setSettStatus(boolean settStatus) {
		this.settStatus = settStatus;
	}

}
