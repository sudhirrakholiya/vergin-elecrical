package com.bonrix.vergin.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="imagegallary")
public class ImageGallary {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "igId")
    private long igId;
    
    @Column(name = "imageName")
    private String imageName;
    
    @Column(name = "imagePath")
    private String imagePath;
    
    @Column(name = "imageStatus")
    private boolean imageStatus;
    
    @Column(name = "imgAddedon")
    private Date imgAddedon;
    
    @Column(name = "imageDesc")
    private String imageDesc;

	public long getIgId() {
		return igId;
	}

	public void setIgId(long igId) {
		this.igId = igId;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public boolean isImageStatus() {
		return imageStatus;
	}

	public void setImageStatus(boolean imageStatus) {
		this.imageStatus = imageStatus;
	}

	public Date getImgAddedon() {
		return imgAddedon;
	}

	public void setImgAddedon(Date imgAddedon) {
		this.imgAddedon = imgAddedon;
	}

	public String getImageDesc() {
		return imageDesc;
	}

	public void setImageDesc(String imageDesc) {
		this.imageDesc = imageDesc;
	}

}
