package com.bonrix.vergin.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="itemcategory",uniqueConstraints={@UniqueConstraint(columnNames={"catName"})})
public class ItemCategory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "catId")
    private long catId;
    
    @Column(name = "catName")
    private String catName;
    
    @Column(name = "parentCatId")
    private long parentCatId;

	public long getCatId() {
		return catId;
	}

	public void setCatId(long catId) {
		this.catId = catId;
	}

	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
	}

	public long getParentCatId() {
		return parentCatId;
	}

	public void setParentCatId(long parentCatId) {
		this.parentCatId = parentCatId;
	}

}
