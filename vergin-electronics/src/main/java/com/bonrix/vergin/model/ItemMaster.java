package com.bonrix.vergin.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "itemmaster",uniqueConstraints={@UniqueConstraint(columnNames={"itemName"})})
public class ItemMaster {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "itemId")
    private long itemId;
	
	@Column(name = "itemName")
    private String itemName;
	
	@Column(name = "itemDesc")
    private String itemDesc;
	
	@Column(name = "itemPrimaryImg")
    private String itemPrimaryImg;
	
	@Column(name = "itemImg1")
    private String itemImg1;
	
	@Column(name = "itemImg2")
    private String itemImg2;
	
	@Column(name = "itemImg3")
    private String itemImg3;
	
	@Column(name = "marketPrice")
	private double marketPrice;
	    
	@Column(name = "offerPrice")
	private double offerPrice;
	
	@Column(name = "minQty")
    private long minQty;
    
    @Column(name = "maxQty")
    private long maxQty;
    
    @Column(name = "avalibleQty")
    private long avalibleQty;
    
    @Column(name = "itemStat")
    private boolean itemStat;
    
    @Column(name = "catId")
    private long catId;
    
    @Column(name = "itemPoints")
	private int itemPoints;

	public long getItemId() {
		return itemId;
	}

	public void setItemId(long itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemDesc() {
		return itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public String getItemPrimaryImg() {
		return itemPrimaryImg;
	}

	public void setItemPrimaryImg(String itemPrimaryImg) {
		this.itemPrimaryImg = itemPrimaryImg;
	}

	public String getItemImg1() {
		return itemImg1;
	}

	public void setItemImg1(String itemImg1) {
		this.itemImg1 = itemImg1;
	}

	public String getItemImg2() {
		return itemImg2;
	}

	public void setItemImg2(String itemImg2) {
		this.itemImg2 = itemImg2;
	}

	public String getItemImg3() {
		return itemImg3;
	}

	public void setItemImg3(String itemImg3) {
		this.itemImg3 = itemImg3;
	}

	public double getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(double marketPrice) {
		this.marketPrice = marketPrice;
	}

	public double getOfferPrice() {
		return offerPrice;
	}

	public void setOfferPrice(double offerPrice) {
		this.offerPrice = offerPrice;
	}

	public long getMinQty() {
		return minQty;
	}

	public void setMinQty(long minQty) {
		this.minQty = minQty;
	}

	public long getMaxQty() {
		return maxQty;
	}

	public void setMaxQty(long maxQty) {
		this.maxQty = maxQty;
	}

	public long getAvalibleQty() {
		return avalibleQty;
	}

	public void setAvalibleQty(long avalibleQty) {
		this.avalibleQty = avalibleQty;
	}

	public boolean isItemStat() {
		return itemStat;
	}

	public void setItemStat(boolean itemStat) {
		this.itemStat = itemStat;
	}

	public long getCatId() {
		return catId;
	}

	public void setCatId(long catId) {
		this.catId = catId;
	}

	public int getItemPoints() {
		return itemPoints;
	}

	public void setItemPoints(int itemPoints) {
		this.itemPoints = itemPoints;
	}
}
