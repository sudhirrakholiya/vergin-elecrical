package com.bonrix.vergin.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "couponmaster")
public class CouponMaster {

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cpnId")
    private long cpnId;
    
    @Column(name = "cpnName")
    private String cpnName;
    
    @Column(name = "cpnCode")
    private String cpnCode;
    
    @Column(name = "cpnPrice")
    private double cpnPrice;
    
    @Column(name = "addedOn")
    private Date addedOn;
    
    @Column(name = "isUsed")
    private boolean isUsed;
    
    @Column(name = "usedByUid")
    private long usedByUid;
    
    @Column(name = "cpnUsedOn")
    private Date cpnUsedOn;
    
    @Column(name = "cpnUniqNum")
    private String cpnUniqNum;

	public long getCpnId() {
		return cpnId;
	}

	public void setCpnId(long cpnId) {
		this.cpnId = cpnId;
	}

	public String getCpnName() {
		return cpnName;
	}

	public void setCpnName(String cpnName) {
		this.cpnName = cpnName;
	}

	public String getCpnCode() {
		return cpnCode;
	}

	public void setCpnCode(String cpnCode) {
		this.cpnCode = cpnCode;
	}

	public Date getAddedOn() {
		return addedOn;
	}

	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	public boolean isUsed() {
		return isUsed;
	}

	public void setUsed(boolean isUsed) {
		this.isUsed = isUsed;
	}

	public long getUsedByUid() {
		return usedByUid;
	}

	public void setUsedByUid(long usedByUid) {
		this.usedByUid = usedByUid;
	}

	public Date getCpnUsedOn() {
		return cpnUsedOn;
	}

	public void setCpnUsedOn(Date cpnUsedOn) {
		this.cpnUsedOn = cpnUsedOn;
	}

	public double getCpnPrice() {
		return cpnPrice;
	}

	public void setCpnPrice(double cpnPrice) {
		this.cpnPrice = cpnPrice;
	}

	public String getCpnUniqNum() {
		return cpnUniqNum;
	}

	public void setCpnUniqNum(String cpnUniqNum) {
		this.cpnUniqNum = cpnUniqNum;
	}

}
