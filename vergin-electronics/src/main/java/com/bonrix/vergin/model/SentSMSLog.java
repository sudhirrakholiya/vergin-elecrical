package com.bonrix.vergin.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sentsmslog")
public class SentSMSLog {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "smlgId")
    private long smlgId;
    
    @Column(name = "mobileNo")
    private String mobileNo;
    
    @Column(name = "message")
    private String message;
    
    @Column(name = "sendDate")
    private Date sendDate;
    
    @Column(name = "APIResponse")
    private String APIResponse;
    
    @Column(name = "messageStatus")
    private String messageStatus;

	public long getSmlgId() {
		return smlgId;
	}

	public void setSmlgId(long smlgId) {
		this.smlgId = smlgId;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public String getAPIResponse() {
		return APIResponse;
	}

	public void setAPIResponse(String aPIResponse) {
		APIResponse = aPIResponse;
	}

	public String getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}

}
