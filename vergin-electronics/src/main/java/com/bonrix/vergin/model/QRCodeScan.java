package com.bonrix.vergin.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "qrcodescan")
public class QRCodeScan {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "autoNo")
    private long autoNo;
    
	@Column(name = "userId")
	private long userId;
	 
    @Column(name = "cpnCode")
    private String cpnCode;
    
    @Column(name = "codeAmt")
	private double codeAmt;
    
    @Column(name = "addedOn")
    private Date addedOn;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getCpnCode() {
		return cpnCode;
	}

	public void setCpnCode(String cpnCode) {
		this.cpnCode = cpnCode;
	}

	public double getCodeAmt() {
		return codeAmt;
	}

	public void setCodeAmt(double codeAmt) {
		this.codeAmt = codeAmt;
	}

	public Date getAddedOn() {
		return addedOn;
	}

	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	public long getAutoNo() {
		return autoNo;
	}

	public void setAutoNo(long autoNo) {
		this.autoNo = autoNo;
	}
    
    
}
