package com.bonrix.vergin.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "feedback")
public class Feedback {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "fdId")
    private long fdId;
	
	@Column(name = "userId")
    private long userId;
	
	@Column(name = "fdStatus")
    private String fdStatus;
	
	@Column(name = "fdMessage")
    private String fdMessage;
	
	@Column(name = "addedon")
    private Date addedon;

	public long getFdId() {
		return fdId;
	}

	public void setFdId(long fdId) {
		this.fdId = fdId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getFdStatus() {
		return fdStatus;
	}

	public void setFdStatus(String fdStatus) {
		this.fdStatus = fdStatus;
	}

	public String getFdMessage() {
		return fdMessage;
	}

	public void setFdMessage(String fdMessage) {
		this.fdMessage = fdMessage;
	}

	public Date getAddedon() {
		return addedon;
	}

	public void setAddedon(Date addedon) {
		this.addedon = addedon;
	}
}
