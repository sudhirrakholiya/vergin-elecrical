package com.bonrix.vergin.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="smstemplate")
public class SMSTemplate {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "tempId")
    private long tempId;
    
    @Column(name = "tempName")
    private String tempName;
    
    @Column(name = "tempMessage")
    private String tempMessage;
    
    @Column(name = "tempStatus")
    private boolean tempStatus;

	public long getTempId() {
		return tempId;
	}

	public void setTempId(long tempId) {
		this.tempId = tempId;
	}

	public String getTempName() {
		return tempName;
	}

	public void setTempName(String tempName) {
		this.tempName = tempName;
	}

	public String getTempMessage() {
		return tempMessage;
	}

	public void setTempMessage(String tempMessage) {
		this.tempMessage = tempMessage;
	}

	public boolean isTempStatus() {
		return tempStatus;
	}

	public void setTempStatus(boolean tempStatus) {
		this.tempStatus = tempStatus;
	}

}
