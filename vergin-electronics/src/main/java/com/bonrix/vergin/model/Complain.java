package com.bonrix.vergin.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "complain")
public class Complain {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cpId")
    private long cpId;
	
	@Column(name = "userId")
    private long userId;
	
	@Column(name = "cpStatus")
    private String cpStatus;
	
	@Column(name = "cpMessage")
    private String cpMessage;
	
	@Column(name = "adminMsg")
    private String adminMsg;
	
	@Column(name = "addedon")
    private Date addedon;
	
	@Column(name = "videoName")
    private String videoName;
	
	@Column(name = "voiceName")
    private String voiceName;

	public long getCpId() {
		return cpId;
	}

	public void setCpId(long cpId) {
		this.cpId = cpId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getCpStatus() {
		return cpStatus;
	}

	public void setCpStatus(String cpStatus) {
		this.cpStatus = cpStatus;
	}

	public String getCpMessage() {
		return cpMessage;
	}

	public void setCpMessage(String cpMessage) {
		this.cpMessage = cpMessage;
	}

	public String getAdminMsg() {
		return adminMsg;
	}

	public void setAdminMsg(String adminMsg) {
		this.adminMsg = adminMsg;
	}

	public Date getAddedon() {
		return addedon;
	}

	public void setAddedon(Date addedon) {
		this.addedon = addedon;
	}

	public String getVideoName() {
		return videoName;
	}

	public void setVideoName(String videoName) {
		this.videoName = videoName;
	}

	public String getVoiceName() {
		return voiceName;
	}

	public void setVoiceName(String voiceName) {
		this.voiceName = voiceName;
	}

}
