package com.bonrix.vergin.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="extradetails",uniqueConstraints={@UniqueConstraint(columnNames={"paramName"})})
public class ExtraDetails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "autoNo")
    private long autoNo;
    
    @Column(name = "paramName")
    private String paramName;
    
    @Column(name = "description")
    private String description;

	public long getAutoNo() {
		return autoNo;
	}

	public void setAutoNo(long autoNo) {
		this.autoNo = autoNo;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
