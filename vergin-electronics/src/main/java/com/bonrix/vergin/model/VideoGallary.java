package com.bonrix.vergin.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="videogallary")
public class VideoGallary {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "vgId")
    private long vgId;
    
    @Column(name = "videoName")
    private String videoName;
    
    @Column(name = "videoURL")
    private String videoURL;
    
    @Column(name = "videoStatus")
    private boolean videoStatus;
    
    @Column(name = "videoAddedon")
    private Date videoAddedon;
    
    @Column(name = "videoDesc")
    private String videoDesc;

	public long getVgId() {
		return vgId;
	}

	public void setVgId(long vgId) {
		this.vgId = vgId;
	}

	public String getVideoName() {
		return videoName;
	}

	public void setVideoName(String videoName) {
		this.videoName = videoName;
	}

	public String getVideoURL() {
		return videoURL;
	}

	public void setVideoURL(String videoURL) {
		this.videoURL = videoURL;
	}

	public boolean isVideoStatus() {
		return videoStatus;
	}

	public void setVideoStatus(boolean videoStatus) {
		this.videoStatus = videoStatus;
	}

	public Date getVideoAddedon() {
		return videoAddedon;
	}

	public void setVideoAddedon(Date videoAddedon) {
		this.videoAddedon = videoAddedon;
	}

	public String getVideoDesc() {
		return videoDesc;
	}

	public void setVideoDesc(String videoDesc) {
		this.videoDesc = videoDesc;
	}
    
}
