package com.bonrix.vergin.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "usertransactions")
public class UserTransactions {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "trId")
    private long trId;
	
	@Column(name = "userId")
    private long userId;
    
    @Column(name = "openAmt")
    private double openAmt;
    
    @Column(name = "closeAmt")
    private double closeAmt;
    
    @Column(name = "amount")
    private double amount;
    
    @Column(name = "transType")
    private String transType;
    
    @Column(name = "transDate")
    private Date transDate;
    
    @Column(name = "qrcodeId")
    private long qrcodeId;
    
    @Column(name = "shopingOrderId")
    private long shopingOrderId;
    
    @Column(name = "bankTransId")
    private long bankTransId;

	public long getTrId() {
		return trId;
	}

	public void setTrId(long trId) {
		this.trId = trId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public double getOpenAmt() {
		return openAmt;
	}

	public void setOpenAmt(double openAmt) {
		this.openAmt = openAmt;
	}

	public double getCloseAmt() {
		return closeAmt;
	}

	public void setCloseAmt(double closeAmt) {
		this.closeAmt = closeAmt;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getTransType() {
		return transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}

	public Date getTransDate() {
		return transDate;
	}

	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}

	public long getQrcodeId() {
		return qrcodeId;
	}

	public void setQrcodeId(long qrcodeId) {
		this.qrcodeId = qrcodeId;
	}

	public long getShopingOrderId() {
		return shopingOrderId;
	}

	public void setShopingOrderId(long shopingOrderId) {
		this.shopingOrderId = shopingOrderId;
	}

	public long getBankTransId() {
		return bankTransId;
	}

	public void setBankTransId(long bankTransId) {
		this.bankTransId = bankTransId;
	}

}
