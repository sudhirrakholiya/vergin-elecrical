package com.bonrix.vergin.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "shopingmaster")
public class ShopingMaster {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "shopingId")
    private long shopingId;
	
	@Column(name = "userId")
    private long userId;
	
	@Column(name = "itemId")
    private long itemId;
	
	@Column(name = "itemQty")
    private long itemQty;
	
	@Column(name = "orderAmt")
    private double orderAmt;
	
	@Column(name = "offerPrice")
    private double offerPrice;
	
	@Column(name = "shippingAddress")
    private String shippingAddress;
	
	@Column(name = "city")
    private String city;
	
	@Column(name = "pinCode")
    private String pinCode;
	
	@Column(name = "orderStatus")
    private String orderStatus;
	
	@Column(name = "courierName")
    private String courierName;
	
	@Column(name = "trackNum")
    private String trackNum;
	
	@Column(name = "userStatmentId")
    private long userStatmentId;
	
	@Column(name = "orderDate")
    private Date orderDate;

	public long getShopingId() {
		return shopingId;
	}

	public void setShopingId(long shopingId) {
		this.shopingId = shopingId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getItemId() {
		return itemId;
	}

	public void setItemId(long itemId) {
		this.itemId = itemId;
	}

	public long getItemQty() {
		return itemQty;
	}

	public void setItemQty(long itemQty) {
		this.itemQty = itemQty;
	}

	public double getOrderAmt() {
		return orderAmt;
	}

	public void setOrderAmt(double orderAmt) {
		this.orderAmt = orderAmt;
	}

	public double getOfferPrice() {
		return offerPrice;
	}

	public void setOfferPrice(double offerPrice) {
		this.offerPrice = offerPrice;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getCourierName() {
		return courierName;
	}

	public void setCourierName(String courierName) {
		this.courierName = courierName;
	}

	public String getTrackNum() {
		return trackNum;
	}

	public void setTrackNum(String trackNum) {
		this.trackNum = trackNum;
	}

	public long getUserStatmentId() {
		return userStatmentId;
	}

	public void setUserStatmentId(long userStatmentId) {
		this.userStatmentId = userStatmentId;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

}
