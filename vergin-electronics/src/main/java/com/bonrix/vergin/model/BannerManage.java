package com.bonrix.vergin.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="bannermanage")
public class BannerManage {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "bnrId")
    private long bnrId;
    
    @Column(name = "bnrName")
    private String bnrName;
    
    @Column(name = "bnrStatus")
    private boolean bnrStatus;
    
    @Column(name = "bnrAddedon")
    private Date bnrAddedon;
    
    @Column(name = "imgName")
    private String imgName;
    
    @Column(name = "bnrURL")
    private String bnrURL;

	public long getBnrId() {
		return bnrId;
	}

	public void setBnrId(long bnrId) {
		this.bnrId = bnrId;
	}

	public String getBnrName() {
		return bnrName;
	}

	public void setBnrName(String bnrName) {
		this.bnrName = bnrName;
	}

	public boolean isBnrStatus() {
		return bnrStatus;
	}

	public void setBnrStatus(boolean bnrStatus) {
		this.bnrStatus = bnrStatus;
	}

	public Date getBnrAddedon() {
		return bnrAddedon;
	}

	public void setBnrAddedon(Date bnrAddedon) {
		this.bnrAddedon = bnrAddedon;
	}

	public String getImgName() {
		return imgName;
	}

	public void setImgName(String imgName) {
		this.imgName = imgName;
	}

	public String getBnrURL() {
		return bnrURL;
	}

	public void setBnrURL(String bnrURL) {
		this.bnrURL = bnrURL;
	}

}
