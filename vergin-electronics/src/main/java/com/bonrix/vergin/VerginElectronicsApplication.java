package com.bonrix.vergin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
public class VerginElectronicsApplication {

	public static void main(String[] args) {
		SpringApplication.run(VerginElectronicsApplication.class, args);
	}

}
