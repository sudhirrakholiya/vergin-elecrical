package com.bonrix.vergin.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.query.Param;

import com.bonrix.vergin.model.SMSSetting;

public interface SMSSettingService {
	
	void saveSMSSetting(SMSSetting ss);
	
	Optional<SMSSetting> getSMSSettingById(long id);
	
	List<SMSSetting> getSMSSettingList();
	
	String deleteSMSSettingById(long id);
	
	String updateSMSSetting(SMSSetting ss);
	
	List<Object[]> settingListData();
	
	void updateSMSSettStat(boolean stat,long setId);
	
	void updateSMSSettStatOther(long setId);

}
