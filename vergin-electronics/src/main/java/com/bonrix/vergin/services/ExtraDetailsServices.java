package com.bonrix.vergin.services;

import com.bonrix.vergin.model.ExtraDetails;

public interface ExtraDetailsServices {

	void saveExtraDetails(ExtraDetails ed);
	
	String deleteDataById(long Id);
	
	void updateExtraDetails(ExtraDetails ed);
}
