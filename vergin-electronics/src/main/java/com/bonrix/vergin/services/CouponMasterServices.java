package com.bonrix.vergin.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.query.Param;

import com.bonrix.vergin.model.CouponMaster;

public interface CouponMasterServices {

	void saveCoupon(CouponMaster cm);
	
	Optional<CouponMaster> getCouponDetailsById(long id);
	
	List<CouponMaster> getCouponList();
	
	String deleteCouponById(long id);
	
	String generateAlhaNumericCoupon(int n);
	
	List<Object[]> getCouponListData();
	
	void updateCouponUse(String couponCode,long uid,Date usedt);
	
	void updateCouponUseAdmin(long couponId,long uid,Date usedt);
	
	void updateCouponLastUnqNum(String lsatUNQNum);
	
	void deleteCouponMsterAll();
	
	void deleteCouponMsterByRange(String startRng, String endRng);
}
