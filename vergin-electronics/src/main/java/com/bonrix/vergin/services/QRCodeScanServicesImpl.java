package com.bonrix.vergin.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bonrix.vergin.model.QRCodeScan;
import com.bonrix.vergin.repository.QRCodeScanRepository;

@Service("qrcodescanservices")
public class QRCodeScanServicesImpl implements QRCodeScanServices{
	
	@Autowired
	QRCodeScanRepository qrcodescanrepository;

	@Override
	public void saveqQrCodeScanData(QRCodeScan qrs) {
		qrcodescanrepository.save(qrs);
	}

}
