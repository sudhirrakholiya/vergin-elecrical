package com.bonrix.vergin.services;

import java.util.List;

import com.bonrix.vergin.model.ItemCategory;

public interface ItemCategoryServices {
	
	void saveItemCategory(ItemCategory ic);
	
	List<ItemCategory> getAllItemCategory();
	
	void updateItemCategory(ItemCategory ic);
	
	void deleteItemCat(long id);

}
