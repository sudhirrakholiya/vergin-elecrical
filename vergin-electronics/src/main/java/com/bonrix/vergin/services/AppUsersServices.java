package com.bonrix.vergin.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.query.Param;

import com.bonrix.vergin.model.AppUsers;

public interface AppUsersServices {
	
	void registerUser(AppUsers au);

	Optional<AppUsers> getUserDetailsById(long id);
	
	List<AppUsers> getUserList();
	
	String updateAppUsers(AppUsers au);

	String deleteAppUsersById(long id);
	
	int userCountByMobile(String sqlQuery);
	
	List<Object[]> getDataByFilter(String Query);
	
	public List<String> gettocken();
	
	public List<String> getSingleTocken(long userId);
	
	void updateAdminPassword(String userName,String password);
	
	public String ConvertDataToWebJSON(String queryCount, String query, int page, int listSize);
	
}
