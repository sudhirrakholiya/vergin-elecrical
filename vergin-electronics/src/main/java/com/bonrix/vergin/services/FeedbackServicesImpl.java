package com.bonrix.vergin.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bonrix.vergin.model.Feedback;
import com.bonrix.vergin.repository.FeedbackRepository;

@Service("feedbackservices")
public class FeedbackServicesImpl implements FeedbackServices{
	
	@Autowired
	FeedbackRepository feedbackrepository;

	@Override
	public void saveFeedback(Feedback fd) {
		feedbackrepository.save(fd);
	}

	@Override
	public void deleteFeedback(long fdId) {
		feedbackrepository.deleteById(fdId);
	}

}
