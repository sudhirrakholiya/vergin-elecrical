package com.bonrix.vergin.services;

import java.util.List;

public interface CommonService {

	public Object getSingleObject(String query);

	int updateData(String query);
}
