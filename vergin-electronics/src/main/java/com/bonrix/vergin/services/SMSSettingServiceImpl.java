package com.bonrix.vergin.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bonrix.vergin.model.SMSSetting;
import com.bonrix.vergin.model.SpringException;
import com.bonrix.vergin.repository.SMSSettingRepository;

@Transactional
@Service("smssettingservice")
public class SMSSettingServiceImpl implements SMSSettingService{
	
	@Autowired
	SMSSettingRepository smsrettingrepository;

	@Override
	public void saveSMSSetting(SMSSetting ss) {
		smsrettingrepository.save(ss);
	}

	@Override
	public Optional<SMSSetting> getSMSSettingById(long id) {
		return smsrettingrepository.findById(id);
	}

	@Override
	public List<SMSSetting> getSMSSettingList() {
		return smsrettingrepository.findAll();
	}

	@Override
	public String deleteSMSSettingById(long id) {
		smsrettingrepository.deleteById(id);
		return new SpringException(true, "DELETED").toString();
	}

	@Override
	public String updateSMSSetting(SMSSetting ss) {
		smsrettingrepository.saveAndFlush(ss);
		return new SpringException(true, "UPDATED").toString();
	}

	@Override
	public List<Object[]> settingListData() {
		return smsrettingrepository.settingListData();
	}

	@Override
	public void updateSMSSettStat(boolean stat, long setId) {
		smsrettingrepository.updateSMSSettStat(stat, setId);
	}

	@Override
	public void updateSMSSettStatOther(long setId) {
		smsrettingrepository.updateSMSSettStatOther(setId);
	}

}
