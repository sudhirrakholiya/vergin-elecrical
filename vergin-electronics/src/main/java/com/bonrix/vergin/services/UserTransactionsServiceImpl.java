package com.bonrix.vergin.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bonrix.vergin.model.UserTransactions;
import com.bonrix.vergin.repository.UserTransactionsRepository;

@Service("usertransactionsservice")
public class UserTransactionsServiceImpl implements UserTransactionsService{
	
	@Autowired
	UserTransactionsRepository usertransactionsrepository;

	@Override
	public void saveTarnsaction(UserTransactions ut) {
		usertransactionsrepository.save(ut);
	}

}
