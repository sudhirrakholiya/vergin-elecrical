package com.bonrix.vergin.services;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Service
public class HibernateService {

    @PersistenceContext
    private EntityManager entityManager;

    public <R> NativeQuery<R> createNativeQuery(String sqlString, Class<R> resultClass) {
        return getSession().createNativeQuery(sqlString, resultClass);
    }

    public NativeQuery createNativeQuery(String sqlString) {
        return getSession().createNativeQuery(sqlString);
    }

    public Session getSession() {
        return entityManager.unwrap(Session.class);
    }
    
    public String ConvertDataToWebJSON(String queryCount, String query, int page, int listSize) {
		Query q = getSession().createSQLQuery(queryCount);
		BigInteger count = (BigInteger) q.uniqueResult();

		List lst = getSession().createSQLQuery(query).setMaxResults(listSize)
				.setFirstResult((page - 1) * listSize).list();

		com.bonrix.vergin.model.DatatableJsonObject pjo = new com.bonrix.vergin.model.DatatableJsonObject();
		pjo.setRecordsFiltered(count.intValue());
		pjo.setRecordsTotal(count.intValue());
		pjo.setData(lst);

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(pjo).toString();
	}
}