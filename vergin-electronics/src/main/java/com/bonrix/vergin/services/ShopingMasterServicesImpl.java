package com.bonrix.vergin.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bonrix.vergin.model.ShopingMaster;
import com.bonrix.vergin.repository.ShopingMasterRepository;

@Service("shopingmasterservices")
public class ShopingMasterServicesImpl implements ShopingMasterServices{
	
	@Autowired
	ShopingMasterRepository shopingmasterrepository;

	@Override
	public void saveOrder(ShopingMaster sm) {
		shopingmasterrepository.save(sm);
	}

	@Override
	public void updateOrder(ShopingMaster sm) {
		shopingmasterrepository.saveAndFlush(sm);
	}

}
