package com.bonrix.vergin.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bonrix.vergin.model.SpringException;
import com.bonrix.vergin.model.VideoGallary;
import com.bonrix.vergin.repository.VideoGallaryRepository;

@Service("videogallaryservices")
@Transactional
public class VideoGallaryServicesImpl implements VideoGallaryServices{
	
	@Autowired
	VideoGallaryRepository videogallaryrepository;

	@Override
	public void saveVideo(VideoGallary vg) {
		videogallaryrepository.save(vg);
	}

	@Override
	public List<VideoGallary> getVideoGallaryList() {
		return videogallaryrepository.findAll();
	}

	@Override
	public Optional<VideoGallary> getVideoGallaryDetailsById(long id) {
		return videogallaryrepository.findById(id);
	}

	@Override
	public String deleteVideoGallaryById(long id) {
		videogallaryrepository.deleteById(id);
		return new SpringException(true, "DELETED").toString();
	}

	@Override
	public List<Object[]> getVideoGallaryListData() {
		return videogallaryrepository.getVideoGallaryListData();
	}

	@Override
	public void updateVideoStat(boolean videoStatus, long id) {
		videogallaryrepository.updateVideoStat(videoStatus, id);
	}

}
