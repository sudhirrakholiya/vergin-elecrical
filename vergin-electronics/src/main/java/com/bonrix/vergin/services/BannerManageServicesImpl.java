package com.bonrix.vergin.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bonrix.vergin.model.BannerManage;
import com.bonrix.vergin.model.SpringException;
import com.bonrix.vergin.repository.BannerManageRepository;

@Service("bannermanageservices")
@Transactional
public class BannerManageServicesImpl implements BannerManageServices{
	
	@Autowired
	BannerManageRepository bannermanagerepository;
	
	@Override
	public void saveBanner(BannerManage bm) {
		bannermanagerepository.save(bm);
	}

	@Override
	public List<BannerManage> getBannerList() {
		return bannermanagerepository.findAll();
	}

	@Override
	public Optional<BannerManage> getBannerDetailsById(long id) {
		return bannermanagerepository.findById(id);
	}

	@Override
	public String deleteBannerById(long id) {
		bannermanagerepository.deleteById(id);
		return new SpringException(true , "DELETED").toString();
	}

	@Override
	public List<Object[]> getBannerListData() {
		return bannermanagerepository.getBannerListData();
	}

	@Override
	public void updateBannerStat(boolean bnrStatus,long bnrId) {
		bannermanagerepository.updateBannerStat(bnrStatus,bnrId);
	}

}
