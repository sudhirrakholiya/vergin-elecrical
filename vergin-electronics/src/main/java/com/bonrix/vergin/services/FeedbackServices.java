package com.bonrix.vergin.services;

import com.bonrix.vergin.model.Feedback;

public interface FeedbackServices {
	
	void saveFeedback(Feedback fd);
	
	void deleteFeedback(long fdId);

}
