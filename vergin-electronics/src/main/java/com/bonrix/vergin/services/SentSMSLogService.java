package com.bonrix.vergin.services;

import java.util.List;

import com.bonrix.vergin.model.SentSMSLog;

public interface SentSMSLogService {
	
	void saveSMSLog(SentSMSLog sl);

	List<SentSMSLog> getSentSMSLogData();
	
	List<Object[]> getSMSLogList();
}
