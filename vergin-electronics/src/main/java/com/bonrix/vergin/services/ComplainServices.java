package com.bonrix.vergin.services;

import com.bonrix.vergin.model.Complain;

public interface ComplainServices {
	
	void addComplain(Complain cp);
	
	void deleteComplain(long cpId);

}
