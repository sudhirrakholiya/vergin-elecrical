package com.bonrix.vergin.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bonrix.vergin.model.AppUsers;
import com.bonrix.vergin.model.SpringException;
import com.bonrix.vergin.repository.AppUsersRepository;

@Service("acntmasterservices")
@Transactional
public class AppUsersServicesImpl implements AppUsersServices{
	
	@Autowired
	AppUsersRepository appusersrepository;
	
	@Autowired
	HibernateService hibernateservice;

	@Override
	public void registerUser(AppUsers au) {
		appusersrepository.save(au);
	}

	@Override
	public Optional<AppUsers> getUserDetailsById(long id) {
		return appusersrepository.findById(id);
	}

	@Override
	public List<AppUsers> getUserList() {
		return appusersrepository.findAll();
	}

	@Override
	public String updateAppUsers(AppUsers au) {
		 appusersrepository.saveAndFlush(au);
		 return new SpringException(true, "Updated").toString();
	}

	@Override
	public String deleteAppUsersById(long id) {
		appusersrepository.deleteById(id);
		return new SpringException(true, "Deleted").toString();
	}

	@Override
	public int userCountByMobile(String sqlQuery) {
		List list= hibernateservice.createNativeQuery(sqlQuery).list();
		return Integer.parseInt(list.get(0).toString());
	}

	@Override
	public List<Object[]> getDataByFilter(String Query) {
		return hibernateservice.createNativeQuery(Query).list();
	}

	@Override
	public List<String> gettocken() {
		return appusersrepository.gettocken();
	}

	@Override
	public List<String> getSingleTocken(long userId) {
		return appusersrepository.getSingleTocken(userId);
	}

	@Override
	public void updateAdminPassword(String userName, String password) {
		appusersrepository.updateAdminPassword(userName, password);
	}

	@Override
	public String ConvertDataToWebJSON(String queryCount, String query, int page, int listSize) {
		return hibernateservice.ConvertDataToWebJSON(queryCount, query, page, listSize);
	}


}
