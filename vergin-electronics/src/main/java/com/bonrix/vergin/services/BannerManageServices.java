package com.bonrix.vergin.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.query.Param;

import com.bonrix.vergin.model.BannerManage;

public interface BannerManageServices {
	
	void saveBanner(BannerManage bm);
	
	List<BannerManage> getBannerList();
	
	Optional<BannerManage> getBannerDetailsById(long id);
	
	String deleteBannerById(long id);
	
	List<Object[]> getBannerListData();

	void updateBannerStat(boolean bnrStatus,long bnrId);
}
