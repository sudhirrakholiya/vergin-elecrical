package com.bonrix.vergin.services;

import java.util.List;
import java.util.Optional;

import com.bonrix.vergin.model.SMSTemplate;

public interface SMSTemplateService {
	
	void saveSMSTemplate(SMSTemplate ss);
	
	Optional<SMSTemplate> getSMSTemplateById(long id);
	
	List<SMSTemplate> getSMSTemplateList();
	
	String deleteSMSTemplateById(long id);
	
	String updateSMSTemplate(SMSTemplate ss);
	
	List<Object[]> SMSTemplateListData();
	
	void updateSMSTempStat(boolean stat,long setId);
	
	void updateSMSTempStatOther(long setId);

}
