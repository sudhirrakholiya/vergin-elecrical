package com.bonrix.vergin.services;

import java.util.List;
import java.util.Optional;

import com.bonrix.vergin.model.VideoGallary;

public interface VideoGallaryServices {

	
	void saveVideo(VideoGallary vg);
	
	List<VideoGallary> getVideoGallaryList();
	
	Optional<VideoGallary> getVideoGallaryDetailsById(long id);
	
	String deleteVideoGallaryById(long id);
	
	List<Object[]> getVideoGallaryListData();

	void updateVideoStat(boolean videoStatus,long id);
}
