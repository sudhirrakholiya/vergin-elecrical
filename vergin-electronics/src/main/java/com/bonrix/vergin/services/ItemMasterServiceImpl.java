package com.bonrix.vergin.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bonrix.vergin.model.ItemMaster;
import com.bonrix.vergin.model.SpringException;
import com.bonrix.vergin.repository.ItemMasterRepository;

@Service("itemmasterervice")
public class ItemMasterServiceImpl implements ItemMasterService{
	
	@Autowired
	ItemMasterRepository itemmasterrepository;

	@Override
	public void addItem(ItemMaster im) {
		itemmasterrepository.save(im);
	}

	@Override
	public String deleteItemById(long id) {
		itemmasterrepository.deleteById(id);
		return new SpringException(true, "DELETED").toString();
	}

	@Override
	public String updateItem(ItemMaster im) {
		itemmasterrepository.saveAndFlush(im);
		return new SpringException(true, "UPDATED").toString();
	}

}
