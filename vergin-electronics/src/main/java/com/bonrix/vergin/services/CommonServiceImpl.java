package com.bonrix.vergin.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

@Service("CommonService")
@Transactional
public class CommonServiceImpl implements CommonService{
	
	@Autowired
	SessionFactory sessionFactory;
	
	@PersistenceContext
    private EntityManager em;

	@Override
	public Object getSingleObject(String query) {
		Query query2 = sessionFactory.openSession().createQuery(query);
		return query2.uniqueResult();
	}
	
	@Override
	@Transactional
	public int updateData(String query) {
		Query  query2=	 (Query) ((Session) em).createSQLQuery(query);	
		return query2.executeUpdate();
	}

}
