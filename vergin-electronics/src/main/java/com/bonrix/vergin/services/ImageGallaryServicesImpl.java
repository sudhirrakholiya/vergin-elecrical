package com.bonrix.vergin.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bonrix.vergin.model.ImageGallary;
import com.bonrix.vergin.repository.ImageGallaryRepository;

@Service("imagegallaryservices")
public class ImageGallaryServicesImpl implements ImageGallaryServices{
	
	@Autowired
	ImageGallaryRepository imagegallaryrepository;

	@Override
	public void saveImageGallaryData(ImageGallary ig) {
		imagegallaryrepository.save(ig);
	}

}
