package com.bonrix.vergin.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bonrix.vergin.model.SMSTemplate;
import com.bonrix.vergin.model.SpringException;
import com.bonrix.vergin.repository.SMSTemplateRepository;

@Service("smstemplateservice")
public class SMSTemplateServiceImpl implements SMSTemplateService{

	@Autowired
	SMSTemplateRepository smstemplaterepository;
	
	@Override
	public void saveSMSTemplate(SMSTemplate ss) {
		smstemplaterepository.save(ss);
	}

	@Override
	public Optional<SMSTemplate> getSMSTemplateById(long id) {
		return smstemplaterepository.findById(id);
	}

	@Override
	public List<SMSTemplate> getSMSTemplateList() {
		return smstemplaterepository.findAll();
	}

	@Override
	public String deleteSMSTemplateById(long id) {
		smstemplaterepository.deleteById(id);
		return new SpringException(true, "DELETED").toString();
	}

	@Override
	public String updateSMSTemplate(SMSTemplate ss) {
		smstemplaterepository.saveAndFlush(ss);
		return new SpringException(true, "UPDATED").toString();
	}

	@Override
	public List<Object[]> SMSTemplateListData() {
		return smstemplaterepository.smstemplateListData();
	}

	@Override
	public void updateSMSTempStat(boolean stat, long tempId) {
		smstemplaterepository.updatesmstempStat(stat, tempId);
	}

	@Override
	public void updateSMSTempStatOther(long setId) {
		smstemplaterepository.updatesmstempOther(setId);
	}

}
