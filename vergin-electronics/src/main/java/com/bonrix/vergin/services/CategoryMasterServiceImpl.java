package com.bonrix.vergin.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bonrix.vergin.model.CategoryMaster;
import com.bonrix.vergin.model.SpringException;
import com.bonrix.vergin.repository.CategoryMasterRepository;

@Service("categorymasterservice")
public class CategoryMasterServiceImpl implements CategoryMasterService{
	
	@Autowired
	CategoryMasterRepository categorymasterrepository;

	@Override
	public void saveCategoty(CategoryMaster cm) {
		categorymasterrepository.save(cm);
	}

	@Override
	public Optional<CategoryMaster> getCategoryMasterDetailsById(long id) {
		return categorymasterrepository.findById(id);
	}

	@Override
	public List<CategoryMaster> getCategoryMasterList() {
		return categorymasterrepository.findAll();
	}

	@Override
	public String updateCategoryMaster(CategoryMaster cm) {
		categorymasterrepository.saveAndFlush(cm);
		return new SpringException(true, "UPDATED").toString();
	}

	@Override
	public String deleteCategoryMasterById(long id) {
		categorymasterrepository.deleteById(id);
		return new SpringException(true, "DELETED").toString();
	}

	@Override
	public List<Object[]> getCategoryMasterData() {
		return categorymasterrepository.getCategoryMasterData();
	}

	@Override
	public void updateDocStat(long catId) {
		categorymasterrepository.updateDocStat(catId);
	}

	@Override
	public List<Object[]> getCategoryMasterDataByParentId(long parentCatId) {
		return categorymasterrepository.getCategoryMasterDataByParentId(parentCatId);
	}

	@Override
	public void deleteCatByParent(long catId) {
		categorymasterrepository.deleteCatByParent(catId);
	}

	@Override
	public List<Object[]> getDashboardData() {
		return categorymasterrepository.getDashboardData();
	}

}
