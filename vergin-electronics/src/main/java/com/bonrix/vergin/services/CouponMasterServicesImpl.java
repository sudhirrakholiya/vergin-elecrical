package com.bonrix.vergin.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bonrix.vergin.model.CouponMaster;
import com.bonrix.vergin.model.SpringException;
import com.bonrix.vergin.repository.CouponMasterRepository;

@Service("couponmasterservices")
@Transactional
public class CouponMasterServicesImpl implements CouponMasterServices{
	
	@Autowired
	CouponMasterRepository couponMasterrepository;

	@Override
	public void saveCoupon(CouponMaster cm) {
		couponMasterrepository.save(cm);
	}

	@Override
	public Optional<CouponMaster> getCouponDetailsById(long id) {
		return couponMasterrepository.findById(id);
	}

	@Override
	public List<CouponMaster> getCouponList() {
		return couponMasterrepository.findAll();
	}

	@Override
	public String deleteCouponById(long id) {
		couponMasterrepository.deleteById(id);
		return new SpringException(true, "DELETED").toString();
	}

	@Override
	public String generateAlhaNumericCoupon(int n) {
		String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuilder sb = new StringBuilder(n);
		for (int i = 0; i < n; i++) {
			int index = (int)(AlphaNumericString.length() * Math.random());
			sb.append(AlphaNumericString.charAt(index));
		}
		return sb.toString();
	}

	@Override
	public List<Object[]> getCouponListData() {
		return couponMasterrepository.getCouponListData();
	}

	@Override
	public void updateCouponUse(String couponCode, long uid,Date usedt) {
		couponMasterrepository.updateCouponUse(couponCode, uid,usedt);
	}

	@Override
	public void updateCouponUseAdmin(long couponId, long uid, Date usedt) {
		couponMasterrepository.updateCouponUseAdmin(couponId, uid, usedt);
	}

	@Override
	public void updateCouponLastUnqNum(String lsatUNQNum) {
		couponMasterrepository.updateCouponLastUnqNum(lsatUNQNum);
	}

	@Override
	public void deleteCouponMsterAll() {
		couponMasterrepository.deleteAll();
	}

	@Override
	public void deleteCouponMsterByRange(String startRng, String endRng) {
		couponMasterrepository.deleteCouponMsterByRange(startRng, endRng);
	}

}
