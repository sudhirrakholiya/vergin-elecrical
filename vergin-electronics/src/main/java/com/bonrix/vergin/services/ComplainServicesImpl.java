package com.bonrix.vergin.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bonrix.vergin.model.Complain;
import com.bonrix.vergin.repository.ComplainRepository;

@Service("complainservices")
public class ComplainServicesImpl implements ComplainServices{
	
	@Autowired
	ComplainRepository complainrepository;

	@Override
	public void addComplain(Complain cp) {
		complainrepository.save(cp);
	}

	@Override
	public void deleteComplain(long cpId) {
		complainrepository.deleteById(cpId);
	}
}
