package com.bonrix.vergin.services;

import com.bonrix.vergin.model.ItemMaster;

public interface ItemMasterService {
	
	void addItem(ItemMaster im);
	
	String deleteItemById(long id);
	
	String updateItem(ItemMaster im);

}
