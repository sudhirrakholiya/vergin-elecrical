package com.bonrix.vergin.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.query.Param;

import com.bonrix.vergin.model.CategoryMaster;

public interface CategoryMasterService {

	void saveCategoty(CategoryMaster cm);

	Optional<CategoryMaster> getCategoryMasterDetailsById(long id);

	List<CategoryMaster> getCategoryMasterList();

	String updateCategoryMaster(CategoryMaster cm);

	String deleteCategoryMasterById(long id);
	
	List<Object[]> getCategoryMasterData();
	
	void updateDocStat(long catId);
	
	List<Object[]> getCategoryMasterDataByParentId(long parentCatId);
	
	void deleteCatByParent(long catId);
	
	List<Object[]> getDashboardData();
}
