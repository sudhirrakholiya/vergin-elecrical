package com.bonrix.vergin.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bonrix.vergin.model.SentSMSLog;
import com.bonrix.vergin.repository.SentSMSLogRepository;

@Service("sentsmslogservice")
@Transactional
public class SentSMSLogServiceImpl implements SentSMSLogService{
	
	@Autowired
	SentSMSLogRepository SentSMSLogRepository;

	@Override
	public void saveSMSLog(SentSMSLog sl) {
		SentSMSLogRepository.save(sl);
	}

	@Override
	public List<SentSMSLog> getSentSMSLogData() {
		return SentSMSLogRepository.findAll();
	}

	@Override
	public List<Object[]> getSMSLogList() {
		return SentSMSLogRepository.getSMSLogList();
	}

}
