package com.bonrix.vergin.services;

import com.bonrix.vergin.model.ShopingMaster;

public interface ShopingMasterServices {
	
	void saveOrder(ShopingMaster sm);
	
	void updateOrder(ShopingMaster sm);

}
