package com.bonrix.vergin.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bonrix.vergin.model.ExtraDetails;
import com.bonrix.vergin.model.SpringException;
import com.bonrix.vergin.repository.ExtraDetailsRepository;

@Service("extradetailsservices")
public class ExtraDetailsServicesImpl implements ExtraDetailsServices{
	
	@Autowired
	ExtraDetailsRepository extradetailsrepository;

	@Override
	public void saveExtraDetails(ExtraDetails ed) {
		extradetailsrepository.save(ed);
	}

	@Override
	public String deleteDataById(long Id) {
		extradetailsrepository.deleteById(Id);
		return new SpringException(true, "DELETED").toString();
	}

	@Override
	public void updateExtraDetails(ExtraDetails ed) {
		extradetailsrepository.saveAndFlush(ed);
	}

}
