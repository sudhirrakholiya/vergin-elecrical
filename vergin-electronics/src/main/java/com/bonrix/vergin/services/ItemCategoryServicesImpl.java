package com.bonrix.vergin.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bonrix.vergin.model.ItemCategory;
import com.bonrix.vergin.repository.ItemCategoryRepository;

@Service("itemcategoryservices")
public class ItemCategoryServicesImpl implements ItemCategoryServices{
	
	@Autowired
	ItemCategoryRepository itemcategoryrepository;

	@Override
	public void saveItemCategory(ItemCategory ic) {
		itemcategoryrepository.save(ic);
	}

	@Override
	public List<ItemCategory> getAllItemCategory() {
		return itemcategoryrepository.findAll();
	}

	@Override
	public void updateItemCategory(ItemCategory ic) {
		itemcategoryrepository.saveAndFlush(ic);
	}

	@Override
	public void deleteItemCat(long id) {
		itemcategoryrepository.deleteById(id);
	}

}
