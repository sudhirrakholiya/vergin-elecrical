package com.bonrix.vergin.config;
import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;


@Configuration
@ConfigurationProperties(prefix = "spring.datasource.hikari")
public class CommonConfiguration extends HikariConfig {

	/*
	 * @Value("${spring.datasource.url}") private String url;
	 * 
	 * @Value("${spring.datasource.username}") private String username;
	 * 
	 * @Value("${spring.datasource.password}") private String password;
	 */
    
	
    @Bean
    public DataSource dataSource() {
    	return new HikariDataSource(this);
		/*
		 * return DataSourceBuilder.create() .url(url) .username(username)
		 * .password(password).build();
		 */
    }
    
}