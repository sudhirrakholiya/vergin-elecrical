package com.bonrix.vergin.job;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import com.sun.istack.internal.logging.Logger;



public class PushFCMNotifiction {
	public final static String AUTH_KEY_FCM = "AIzaSyAYKFZ7DtQUbmMlNvhAa5l1GyiPsSCYL_c";
	public final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";
	public static final Logger log = Logger.getLogger(PushFCMNotifiction.class);

	
public static String adminsendPushNotificationbytopic(String[] deviceToken,String title,String message,String filepath)throws IOException, JSONException {
    URL url = new URL(API_URL_FCM);
    
    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    conn.setUseCaches(false);
    conn.setDoInput(true);
    conn.setDoOutput(true);
    conn.setRequestMethod("POST");
    conn.setRequestProperty("Authorization", "AAAAzL59nlY:APA91bGzx3M8qwdW9cZU2i_kSTBMqFVIV7j4M_p7hx7bRhTtVQlT3DvAzsM1Q5z7ZiddIWb6obOB3wC4_ImTf7WNbhfboISoH5itEOHjRoK0BRY2RjanRgVL35tAeCpGhUVuHFOkaizT" );
    conn.setRequestProperty("Content-Type", "application/json");
    JSONObject json = new JSONObject();
    json.put("registration_ids",  deviceToken);
    JSONObject info = new JSONObject();
  //info.put("notificationtype", notificationtype);                //String notificationtype,JSONArray notificationData,
    info.put("title", URLEncoder.encode(title,"UTF-8")); // Notification title
    info.put("message", URLEncoder.encode(message,"UTF-8"));
    info.put("image", URLEncoder.encode(filepath,"UTF-8"));// Notification message
    info.put("userid", URLEncoder.encode("N/A","UTF-8"));
  //info.put("notificationData", notificationData);
    info.put("timestamp",new Date().getTime()+"");
    json.put("to", "/topics/sendtoall");
    json.put("data", info);   // body
  //json.put("timestamp",new Date());
    JSONObject payload = new JSONObject();
    json.put("payload", payload);
   log.info("NotifictionHashmap::: SENDJSON::: "+json.toString());
   
    try {
        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
        wr.write(json.toString());
        wr.flush();
        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
        String output;
        log.info("NotifictionHashmap::: Output from Server .... \n");
        while ((output = br.readLine()) != null) {
        log.info("NotifictionHashmap::: FCMResponce::: "+output);
     }
        log.info("NotifictionHashmap::: FCM Notification is sent successfully");
        return "FCM Notification is sent successfully";
    } catch (Exception e) {
    	log.info("PushFCMNotifiction "+e.getMessage());
        e.printStackTrace();
        return "error";
    }
   
}


public static void main(String[] args) {
	/*	try {
			sendPushNotification(device_Id,"hii","everyone","http://tracker24.in/images/icon/truck/red_new/truck0.png");
		} catch (IOException | JSONException e) {
			e.printStackTrace();
		}*/
	}


}