package com.bonrix.vergin.job;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.web.multipart.MultipartFile;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class FTPUploadJob {

	static String serverURL = "http://159.89.175.120/UploadFile/vergin/";
	static String serverIP = "159.89.175.120";
	static String serverUserName = "root";
	static String serverPassword = "9SO3PLg83$25";
	static String serverUploadPath = "/opt/tomcat8/webapps/ROOT/UploadFile/vergin/";
	int serverPort = 22;
	
	public void upload(MultipartFile file) throws IOException, JSchException, SftpException {
		JSch jsch = new JSch();
		Session session = jsch.getSession(serverUserName, serverIP, serverPort); // 22 for SFTP
		session.setPassword(serverPassword);
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);
		session.connect(10000);
		Channel channel = session.openChannel("sftp");
		channel.connect();

		System.out.println("Connection Opened  "+file.getOriginalFilename());
		ChannelSftp channelSftp = (ChannelSftp) channel;
	//	InputStream inputStream = new FileInputStream("C:\\Users\\Sudhir Rakholiya\\Desktop\\ipl.jpg");
		InputStream inputStream =  new BufferedInputStream(file.getInputStream());
		String fileName = serverUploadPath+file.getOriginalFilename();
		channelSftp.put(inputStream, fileName);
		System.out.println("File should be uploaded");
		channelSftp.disconnect();
		session.disconnect();
    }

	
}
