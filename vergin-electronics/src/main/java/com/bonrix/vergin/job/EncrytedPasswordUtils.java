package com.bonrix.vergin.job;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class EncrytedPasswordUtils {
	
	final static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	
	// Encryte Password with BCryptPasswordEncoder
    public static String encrytePassword(String password) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(password);
    }
 
    public static void main(String[] args) {
		
    	
    	Date today = new Date();
    	String strDate = "B_C_"+df.format(today);
    	System.out.println("strDate: " + strDate);
		 
    	  String password = "mandirToVahiBanega"; 
		  String encrytedPassword = encrytePassword(strDate); 
		  System.out.println("Encryted Password: " + encrytedPassword);
		 
    	
	/*	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    	String oldPass = "admin";
    	String newPass = "admin";
    	String encrytedOldPassword = encrytePassword(oldPass); 
    	System.out.println("Test::"+  encoder.matches(newPass, encrytedOldPassword));*/
    	
    }
}