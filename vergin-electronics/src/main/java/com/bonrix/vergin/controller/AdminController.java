package com.bonrix.vergin.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.bonrix.vergin.job.EncrytedPasswordUtils;
import com.bonrix.vergin.job.FTPUploadJob;
import com.bonrix.vergin.job.PushFCMNotifiction;
import com.bonrix.vergin.model.AppUsers;
import com.bonrix.vergin.model.BannerManage;
import com.bonrix.vergin.model.CategoryMaster;
import com.bonrix.vergin.model.CouponMaster;
import com.bonrix.vergin.model.ExtraDetails;
import com.bonrix.vergin.model.ImageGallary;
import com.bonrix.vergin.model.ItemCategory;
import com.bonrix.vergin.model.ItemMaster;
import com.bonrix.vergin.model.SMSSetting;
import com.bonrix.vergin.model.SMSTemplate;
import com.bonrix.vergin.model.ShopingMaster;
import com.bonrix.vergin.model.SpringException;
import com.bonrix.vergin.model.UserTransactions;
import com.bonrix.vergin.model.VideoGallary;
import com.bonrix.vergin.services.AppUsersServices;
import com.bonrix.vergin.services.BannerManageServices;
import com.bonrix.vergin.services.CategoryMasterService;
import com.bonrix.vergin.services.CommonService;
import com.bonrix.vergin.services.ComplainServices;
import com.bonrix.vergin.services.CouponMasterServices;
import com.bonrix.vergin.services.ExtraDetailsServices;
import com.bonrix.vergin.services.FeedbackServices;
import com.bonrix.vergin.services.ImageGallaryServices;
import com.bonrix.vergin.services.ItemCategoryServices;
import com.bonrix.vergin.services.ItemMasterService;
import com.bonrix.vergin.services.SMSSettingService;
import com.bonrix.vergin.services.SMSTemplateService;
import com.bonrix.vergin.services.SentSMSLogService;
import com.bonrix.vergin.services.ShopingMasterServices;
import com.bonrix.vergin.services.UserTransactionsService;
import com.bonrix.vergin.services.VideoGallaryServices;

import lombok.extern.log4j.Log4j2;

@CrossOrigin(origins = "*")
@RestController
@Log4j2
public class AdminController {
	
	@Autowired
	AppUsersServices appusersservices;
	
	@Autowired
	CouponMasterServices couponmasterservices;
	
	@Autowired
	BannerManageServices bannermanageservices;
	
	@Autowired
	VideoGallaryServices videogallaryservices;
	
	@Autowired
	SMSSettingService smssettingservice;
	
	@Autowired
	SMSTemplateService smstemplateservice;
	
	@Autowired
	SentSMSLogService sentsmslogservice;
	
	@Autowired
	CategoryMasterService categorymasterservice;
	
	@Autowired
	ItemMasterService itemmasterservice;
	
	@Autowired
	CommonService commonservice;
	
	@Autowired
	UserTransactionsService usertransactionsservice;
	
	@Autowired
	ImageGallaryServices imagegallaryservices;
	
	@Autowired
	FeedbackServices feedbackservices;
	
	@Autowired
	ComplainServices complainservices;
	
	@Autowired
	ShopingMasterServices shopingmasterservices;
	
	@Autowired
	ExtraDetailsServices extradetailsservices;
	
	@Autowired
	ItemCategoryServices itemcategoryservices;
	
	
	SimpleDateFormat formatter1=new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat formatter2=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	static String serverURL = "http://159.89.175.120/UploadFile/vergin/";
	static String serverIP = "159.89.175.120";
	static String serverUserName = "root";
	static String serverPassword = "9SO3PLg83$25";
	static String serverUploadPath = "/opt/tomcat8/webapps/ROOT/UploadFile/vergin/";
	int serverPort = 22;
	
	
	@GetMapping(value = "/getAllUserList", produces = { "application/json" })
	public List<AppUsers> getAllUserList() throws Exception {
		return appusersservices.getUserList();
	}
	
	@GetMapping(value = "/getUserById/{id}",produces = { "application/json" })
	public Optional<AppUsers> getUserById(@PathVariable long id) {
		return appusersservices.getUserDetailsById(id);
	}
	
	@DeleteMapping(value = "/deleteUser/{id}")
	public String deleteUser(@PathVariable int id) {
		return appusersservices.deleteAppUsersById(id);
	}
	
	
	@GetMapping(value = "/generateCoupon/{couponNum}/{couponAmt}/{couponName}/{cpnStr}",produces = {"application/json"})
	public String generateCoupon(@PathVariable int couponNum, @PathVariable double couponAmt,@PathVariable String couponName,@PathVariable String cpnStr) {
		try {
			long lastUnNum=00000000;
			List<Object[]> couponList = appusersservices.getDataByFilter("SELECT * FROM user WHERE Username='admin'");
			for(Object[] result : couponList) {
				lastUnNum = Long.parseLong(result[4].toString());
			}
			System.out.println("lastUnNum::"+lastUnNum);
			
			if(!(couponNum==0) && !(couponAmt==0)) {
				for (int i = 1; i <= couponNum; i++) {
					String couponCode = couponmasterservices.generateAlhaNumericCoupon(8);
					CouponMaster cm = new CouponMaster();
					cm.setCpnName(couponName+" "+i);
					cm.setCpnCode(couponCode);
					cm.setCpnPrice(couponAmt);
					cm.setUsed(false);
					cm.setUsedByUid(0);
					cm.setAddedOn(new Date());
				//	cm.setAddedOn(formatter2.parse(formatter2.format(new Date())));
					cm.setCpnUsedOn(formatter1.parse("0000-00-00"));
					String var = String.format("%08d", (lastUnNum+1));
					cm.setCpnUniqNum(var);
					couponmasterservices.saveCoupon(cm);
					System.out.println("lastUnNum::"+lastUnNum);
					lastUnNum++;
				}
			}else {
				ArrayList<Integer> mylist = new ArrayList<Integer>();
				String testStr = "25,3,30,5,50,6,100,4";
				String[] arrOfStr = cpnStr.split(",");
				for(int i=0;i<arrOfStr.length;i++) {
					if (i % 2 == 0) {
		              int len = Integer.parseInt(arrOfStr[i+1]);
					  for(int j =0;j<len;j++){
						 mylist.add(Integer.parseInt(arrOfStr[i]));
					  }
		            }
				}
				System.out.println("Original List : \n" + mylist);
				Collections.shuffle(mylist);
				System.out.println("\nShuffled List : \n" + mylist);
				
				for (int i = 0; i < mylist.size(); i++) {
					String couponCode = couponmasterservices.generateAlhaNumericCoupon(8);
					CouponMaster cm = new CouponMaster();
					cm.setCpnName(couponName+" "+i);
					cm.setCpnCode(couponCode);
					cm.setCpnPrice(mylist.get(i));
					cm.setUsed(false);
					cm.setUsedByUid(0);
					cm.setAddedOn(new Date());
					cm.setCpnUsedOn(formatter1.parse("0000-00-00"));
					String var = String.format("%08d", (lastUnNum+1));
					cm.setCpnUniqNum(var);
					couponmasterservices.saveCoupon(cm);
				//	System.out.println("lastUnNum::"+lastUnNum);
					lastUnNum++;
				}
			}
			System.out.println("lastUnNumOUT::"+String.format("%08d", (lastUnNum)));
			couponmasterservices.updateCouponLastUnqNum(String.format("%08d", (lastUnNum)));
			return new SpringException(true, "Coupon Sucessfully Generated").toString();
		}catch (Exception e) {
			e.printStackTrace();
			return new SpringException(false, "Please enter proper details").toString();
		}
	}
	
	@GetMapping(value = "/getAllCouponList", produces = { "application/json" })
	public List<CouponMaster> getAllCouponList() throws Exception {
		return couponmasterservices.getCouponList();
	}
	
	
	@GetMapping(value = "/getAllCouponListData", produces = { "application/json" })
	public String getAllCouponListData(HttpServletRequest request, HttpServletResponse response) throws Exception {
		JSONArray jarray = new JSONArray();
		int counter=1;
		List<Object[]> dataList = couponmasterservices.getCouponListData();
		for(Object[] result : dataList) {
			JSONObject jobj = new JSONObject();
			jobj.put("cpnId", result[0]);
			jobj.put("addedOn",  formatter2.format(formatter2.parse(result[1].toString())));
			jobj.put("cpnCode", result[2]);
			jobj.put("cpnName", result[3]);
	//		jobj.put("cpnUsedOn", formatter1.format(formatter1.parse(result[4].toString())));
	//		jobj.put("IsUsed", result[5]);
			if(result[5].toString().equalsIgnoreCase("true")) {
				jobj.put("IsUsed", "YES");
				jobj.put("cpnUsedOn", formatter1.format(formatter1.parse(result[4].toString())));
			}else {
				jobj.put("IsUsed", "NO");
				jobj.put("cpnUsedOn", "");
			}
			jobj.put("usedByUid", result[6]);
			jobj.put("cpnPrice", result[7]);
			jobj.put("usedBy", result[8]);
		//	jobj.put("autoNo", counter);
		//	String var = String.format("%08d", counter);
	//		System.out.println("var::"+var.replaceAll("....", "$0 "));
			jobj.put("autoNo", result[9].toString().replaceAll("....", "$0 "));
			jarray.put(jobj);
			counter++;
		}
		return jarray.toString();
	}
	
	
	@GetMapping(value = "/getAllCouponListDataNew", produces = { "application/json" })
	public String getAllCouponListDataNew(HttpServletRequest request, HttpServletResponse response) throws Exception {
		JSONArray jarray = new JSONArray();
		
		String orderBy = request.getParameter("orderBy");
		int page = 1;
		int listSize = 10;
		int start = 0;
		int len = 0;
		
		try {
			if (request.getParameter("start") != null) {
				start = Integer.parseInt(request.getParameter("start"));
			}else {

			}
			if (request.getParameter("length") != null) {
				len = Integer.parseInt(request.getParameter("length"));
				listSize = len;
			} else {
				// System.out.println("LENGTH : " + len);
			}
			page = start / len + 1;
		} catch (Exception ex) {

		}
		
		String query = "SELECT cm.cpnId,cm.cpnName,cm.cpnCode,cm.cpnPrice,cm.addedOn,COALESCE(CONCAT(au.firstName,' ', au.lastName), '')  AS userName,cm.cpnUsedOn,cm.isUsed,cm.usedByUid,cm.cpnUniqNum  FROM couponmaster cm  LEFT JOIN appusers au ON cm.usedByUid= au.userId ORDER BY addedOn DESC ";
		String queryCount = "SELECT COUNT(*) FROM couponmaster cm  LEFT JOIN appusers au ON cm.usedByUid= au.userId ORDER BY addedOn DESC";
		
		String list = "";
		list = appusersservices.ConvertDataToWebJSON(queryCount.toString(), "" + query, page, listSize);
		if (list.length() != 0) {
			if (request.getParameter("callback") != null) {
				// System.out.println(request.getParameter("callback") +
				// "(" + list + ");");
				return request.getParameter("callback") + "(" + list + ");";
			} else {
				// System.out.println("IN LIST");
				return list;
			}
		} else {
			// return "[{\"responceCode\":\"No Data Found\"}]";
			return request.getParameter("callback") + "(" + jarray.toString() + ");";
		}
		
	}
	
	
	@DeleteMapping(value = "/deleteCoupon/{id}")
	public String deleteCoupon(@PathVariable long id) {
		return couponmasterservices.deleteCouponById(id);
	}
	
	@GetMapping(value = "/getCouponListById/{id}",produces = { "application/json" })
	public Optional<CouponMaster> getCouponListById(@PathVariable long id) {
		return couponmasterservices.getCouponDetailsById(id);
	}
	
	@GetMapping(value = "/markCouponUse/{couponId}/{userId}",produces = {"application/json"})
	public String markCouponUse(@PathVariable long couponId,@PathVariable long userId) {
		try {
			Date useddt = new Date();
			couponmasterservices.updateCouponUseAdmin(couponId, userId,useddt);
			return new SpringException(true, "Coupon Status Changed").toString();
		}catch (Exception e) {
			e.printStackTrace();
			return new SpringException(false, "Please enter proper details").toString();
		}
	}
	
	@GetMapping(value = "/getCouponsByFilter/{userId}/{stDate}/{enDate}")
	public String getClientsByFilter(@PathVariable long userId,@PathVariable String stDate,@PathVariable String enDate,HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		String orderBy = request.getParameter("orderBy");
		int page = 1;
		int listSize = 10;
		int start = 0;
		int len = 0;
		
		try {
			if (request.getParameter("start") != null) {
				start = Integer.parseInt(request.getParameter("start"));
			}else {

			}
			if (request.getParameter("length") != null) {
				len = Integer.parseInt(request.getParameter("length"));
				listSize = len;
			} else {
				// System.out.println("LENGTH : " + len);
			}
			page = start / len + 1;
		} catch (Exception ex) {

		}
		
		JSONArray jarray = new JSONArray();
		StringBuilder sb = new StringBuilder("SELECT cm.cpnId,cm.cpnName,cm.cpnCode,cm.cpnPrice,cm.addedOn,COALESCE(CONCAT(au.firstName,' ', au.lastName), '')  AS userName,cm.cpnUsedOn,cm.isUsed,cm.usedByUid,cm.cpnUniqNum"
												+ " FROM couponmaster cm  LEFT JOIN appusers au ON cm.usedByUid= au.userId WHERE 0=0");
	    if (!(userId==0)) {
			sb.append(" AND cm.usedByUid="+userId+" ");
		}
	    if (!stDate.equals("NA") && !enDate.equals("NA")) {
			sb.append(" AND (cm.addedOn BETWEEN '"+stDate+"' AND ADDDATE('"+enDate+"', INTERVAL 1 DAY))");
		}
	    
	    String queryCount = "SELECT COUNT(*) FROM couponmaster cm";
	    String list = "";
	    list = appusersservices.ConvertDataToWebJSON(queryCount.toString(), "" + sb.toString(), page, listSize);
		if (list.length() != 0) {
			if (request.getParameter("callback") != null) {
				// System.out.println(request.getParameter("callback") +
				// "(" + list + ");");
				return request.getParameter("callback") + "(" + list + ");";
			} else {
				// System.out.println("IN LIST");
				return list;
			}
		} else {
			// return "[{\"responceCode\":\"No Data Found\"}]";
			return request.getParameter("callback") + "(" + jarray.toString() + ");";
		}
		
	/*    int counter = 1;
		List<Object[]> couponList = appusersservices.getDataByFilter(sb.toString());
		for (Object[] result : couponList) {
			JSONObject jobj = new JSONObject();
			jobj.put("cpnId", result[0]);
			jobj.put("addedOn",  formatter2.format(formatter2.parse(result[1].toString())));
			jobj.put("cpnCode", result[2]);
			jobj.put("cpnName", result[3]);
			jobj.put("cpnUsedOn", formatter1.format(formatter1.parse(result[4].toString())));
			jobj.put("IsUsed", result[5]);
			jobj.put("usedByUid", result[6]);
			jobj.put("cpnPrice", result[7]);
			jobj.put("usedBy", result[8]);
		//	String var = String.format("%08d", counter);
		//	jobj.put("autoNo", var.replaceAll("....", "$0 ").trim());
			jobj.put("autoNo", result[9].toString().replaceAll("....", "$0 "));
			jarray.put(jobj);
			counter++;
		}
		return jarray.toString();*/
	}
	
	@PostMapping(value = "/addBanner", produces = { "application/json" })
	public String addBanner(@RequestParam(value="fileName") MultipartFile file,@RequestParam(value="bnrName") String bnrName) throws Exception {
		FTPUploadJob job = new FTPUploadJob();
		job.upload(file);
		try {
			BannerManage bm = new BannerManage();
			bm.setBnrName(bnrName);
			bm.setBnrAddedon(new Date());
			bm.setBnrStatus(true);
			bm.setImgName(file.getOriginalFilename());
			bannermanageservices.saveBanner(bm);
			return new SpringException(true, "Banner Added Successfully").toString();
		}catch (Exception e) {
			return new SpringException(false, e.getMessage()).toString();
		}
	}
	
	@GetMapping(value = "/getAllBannerList", produces = { "application/json" })
	public List<BannerManage> getAllBannerList() throws Exception {
		return bannermanageservices.getBannerList();
	}
	
	@GetMapping(value = "/getBannerListData")
	public String getBannerListData() throws Exception {
		JSONArray jarray = new JSONArray();
		int counter=1;
		List<Object[]> bannerList = bannermanageservices.getBannerListData();
		for (Object[] result : bannerList) {
			JSONObject jobj = new JSONObject();
			jobj.put("bnrId", result[0]);
			jobj.put("addedOn",  formatter2.format(formatter2.parse(result[1].toString())));
			jobj.put("bnrName", result[2]);
			jobj.put("bnrStatus", result[3]);
			jobj.put("imgName", result[4]);
			jobj.put("autoNo", counter);
			jobj.put("bnrPath", serverURL+result[4]);
			jarray.put(jobj);
			counter++;
		}
		return jarray.toString();
	}
	
	@DeleteMapping(value = "/deleteBanner/{id}")
	public String deleteBanner(@PathVariable long id) {
		return bannermanageservices.deleteBannerById(id);
	}
	
	@DeleteMapping(value = "/updateBannerStat/{id}/{stat}")
	public String updateBannerStat(@PathVariable long id,@PathVariable boolean stat) {
		try {
			bannermanageservices.updateBannerStat(stat, id);
			return new SpringException(true, "Banner Status Changed").toString();
		} catch (Exception e) {
			return new SpringException(false, e.getMessage()).toString();
		}
	}
	
	@PostMapping(value = "/addVideoGallary", produces = { "application/json" })
	public String addVideoGallary(@RequestBody VideoGallary vg) throws Exception {
		try {
			vg.setVideoAddedon(new Date());
			vg.setVideoStatus(true);
			videogallaryservices.saveVideo(vg);
			return new SpringException(true, "Video Added Successfully").toString();
		}catch (Exception e) {
			e.printStackTrace();
			return new SpringException(false, e.getMessage()).toString();
		}
	}
	
	@GetMapping(value = "/getVideoGallaryList" ,produces = { "application/json" })
	public String getVideoGallaryList() throws Exception {
		JSONArray jarray = new JSONArray();
		int counter=1;
		List<Object[]> videoList = videogallaryservices.getVideoGallaryListData();
		for (Object[] result : videoList) {
			JSONObject jobj = new JSONObject();
			jobj.put("vgId", result[0]);
			jobj.put("addedOn",  formatter2.format(formatter2.parse(result[1].toString())));
			jobj.put("vidName", result[2]);
			jobj.put("vidStatus", result[3]);
			jobj.put("vidURL", result[4]);
			jobj.put("vidDesc", result[5]);
			jobj.put("autoNo", counter);
			jarray.put(jobj);
			counter++;
		}
		return jarray.toString();
	}
	
	@DeleteMapping(value = "/deleteVideo/{id}")
	public String deleteVideo(@PathVariable long id) {
		return videogallaryservices.deleteVideoGallaryById(id);
	}
	
	@DeleteMapping(value = "/updateVideoStat/{id}/{stat}")
	public String updateVideoStat(@PathVariable long id,@PathVariable boolean stat) {
		try {
			videogallaryservices.updateVideoStat(stat, id);
			return new SpringException(true, "Video Status Changed").toString();
		} catch (Exception e) {
			return new SpringException(false, e.getMessage()).toString();
		}
	}
	
	@PostMapping(value = "/addSMSSetting", produces = { "application/json" })
	public String addSMSSetting(@RequestBody SMSSetting ss) throws Exception {
		try {
			ss.setSettStatus(false);
			smssettingservice.saveSMSSetting(ss);
			return new SpringException(true, "Setting Added Successfully").toString();
		}catch (Exception e) {
			e.printStackTrace();
			return new SpringException(false, e.getMessage()).toString();
		}
	}
	
	@GetMapping(value = "/getSMSSettingList", produces = { "application/json" })
	public List<SMSSetting> getSMSSettingList() throws Exception {
		return smssettingservice.getSMSSettingList();
	}
	
	@GetMapping(value = "/getSMSSettingById/{id}",produces = { "application/json" })
	public Optional<SMSSetting> getSMSSettingById(@PathVariable long id) {
		return smssettingservice.getSMSSettingById(id);
	}
	
	@DeleteMapping(value = "/deleteSMSSetting/{id}")
	public String deleteSMSSetting(@PathVariable int id) {
		return smssettingservice.deleteSMSSettingById(id);
	}
	
	@PostMapping(value = "/updateSMSSetting", produces = { "application/json" })
	public String updateSMSSetting(@RequestBody SMSSetting ss) throws Exception {
		try {
			smssettingservice.updateSMSSetting(ss);
			return new SpringException(true, "Setting Updated Successfully").toString();
		}catch (Exception e) {
			e.printStackTrace();
			return new SpringException(false, e.getMessage()).toString();
		}
	}
	
	@GetMapping(value = "/getSMSSettingData" ,produces = { "application/json" })
	public String getSMSSettingData() throws Exception {
		JSONArray jarray = new JSONArray();
		int counter=1;
		List<Object[]> smssettList = smssettingservice.settingListData();
		for (Object[] result : smssettList) {
			JSONObject jobj = new JSONObject();
			jobj.put("setId", result[0]);
			jobj.put("apiURL",  result[1].toString().trim());
			jobj.put("provider", result[2]);
			jobj.put("succKey", result[3]);
			jobj.put("settStatus", result[4]);
			jobj.put("autoNo", counter);
			jarray.put(jobj);
			counter++;
		}
		return jarray.toString();
	}
	
	@GetMapping(value = "/updateSettStat/{id}/{stat}")
	public String updateSettStat(@PathVariable long id,@PathVariable boolean stat) {
		try {
			smssettingservice.updateSMSSettStat(stat, id);
			if(stat==true) {
				smssettingservice.updateSMSSettStatOther(id);
			}
			return new SpringException(true, "SMS Setting Status Changed").toString();
		} catch (Exception e) {
			return new SpringException(false, e.getMessage()).toString();
		}
	}
	
	@PostMapping(value = "/addSMSTemplate", produces = { "application/json" })
	public String addSMSTemplate(@RequestBody SMSTemplate ss) throws Exception {
		try {
			ss.setTempStatus(false);
			smstemplateservice.saveSMSTemplate(ss);
			return new SpringException(true, "Template Added Successfully").toString();
		}catch (Exception e) {
			e.printStackTrace();
			return new SpringException(false, e.getMessage()).toString();
		}
	}
	
	@GetMapping(value = "/getSMSTemplateList", produces = { "application/json" })
	public List<SMSTemplate> getSMSTemplateList() throws Exception {
		return smstemplateservice.getSMSTemplateList();
	}
	
	@GetMapping(value = "/getSMSTemplateById/{id}",produces = { "application/json" })
	public Optional<SMSTemplate> getSMSTemplateById(@PathVariable long id) {
		return smstemplateservice.getSMSTemplateById(id);
	}
	
	@DeleteMapping(value = "/deleteSMSTemplate/{id}")
	public String deleteSMSTemplate(@PathVariable int id) {
		return smstemplateservice.deleteSMSTemplateById(id);
	}
	
	@PostMapping(value = "/updateSMSTemplate", produces = { "application/json" })
	public String updateSMSTemplate(@RequestBody SMSTemplate ss) throws Exception {
		try {
			smstemplateservice.updateSMSTemplate(ss);
			return new SpringException(true, "SMSTemplate Updated Successfully").toString();
		}catch (Exception e) {
			e.printStackTrace();
			return new SpringException(false, e.getMessage()).toString();
		}
	}
	
	@GetMapping(value = "/getSMSTemplateData" ,produces = { "application/json" })
	public String getSMSTemplateData() throws Exception {
		JSONArray jarray = new JSONArray();
		int counter=1;
		List<Object[]> smstempList = smstemplateservice.SMSTemplateListData();
		for (Object[] result : smstempList) {
			JSONObject jobj = new JSONObject();
			jobj.put("tempId", result[0]);
			jobj.put("tempMessage",  result[1].toString().trim());
			jobj.put("tempName", result[2]);
			jobj.put("tempStat", result[3]);
			jobj.put("autoNo", counter);
			jarray.put(jobj);
			counter++;
		}
		return jarray.toString();
	}
	
	@GetMapping(value = "/updateTempStat/{id}/{stat}")
	public String updateTempStat(@PathVariable long id,@PathVariable boolean stat) {
		try {
			smstemplateservice.updateSMSTempStat(stat, id);
			if(stat==true) {
				smstemplateservice.updateSMSTempStatOther(id);
			}
			return new SpringException(true, "SMS Setting Status Changed").toString();
		} catch (Exception e) {
			return new SpringException(false, e.getMessage()).toString();
		}
	}
	
	
	@GetMapping(value = "/getSMSLogData" ,produces = { "application/json" })
	public String getSMSLogData() throws Exception {
		JSONArray jarray = new JSONArray();
		int counter=1;
		List<Object[]> smslogList = sentsmslogservice.getSMSLogList();
		for (Object[] result : smslogList) {
			JSONObject jobj = new JSONObject();
			jobj.put("smslgId", result[0]);
			jobj.put("apiResp",  result[1].toString().trim());
			jobj.put("message", result[2]);
			jobj.put("messageStat", result[3]);
			jobj.put("mobileNo", result[4]);
			jobj.put("sendDate", formatter2.format(formatter2.parse(result[5].toString())));
			jobj.put("autoNo", counter);
			jarray.put(jobj);
			counter++;
		}
		return jarray.toString();
	}
	
	
	@PostMapping(value = "/sendNotification", produces = { "application/json" })
	public String sendNotification(@RequestParam(value="fileName") MultipartFile file,@RequestParam(value="notiTitle") String notiTitle,
			                       @RequestParam(value="notiMsg") String notiMsg) throws Exception {
		
		FTPUploadJob job = new FTPUploadJob();
		job.upload(file);
		String imagePath = serverURL+file.getOriginalFilename();
		List<String> tockenlist = appusersservices.gettocken();
		String tocken[] = tockenlist.stream().toArray(String[]::new);
		System.out.println("tocken::" + tocken.length);
		PushFCMNotifiction pm = new PushFCMNotifiction();
		String resp = null;
		try {
			resp = pm.adminsendPushNotificationbytopic(tocken,notiTitle, notiMsg, imagePath);
			return new SpringException(true, resp).toString();
		}catch (Exception e) {
			return new SpringException(false, e.getMessage()).toString();
		}
	}
	
	@PostMapping(value = "/sendSingleUserNotification", produces = { "application/json" })
	public String sendSingleUserNotification(@RequestParam(value="fileName") MultipartFile file,@RequestParam(value="notiTitle") String notiTitle,
			                       @RequestParam(value="notiMsg") String notiMsg,@RequestParam(value = "userId") long userId) throws Exception {
		
		FTPUploadJob job = new FTPUploadJob();
		job.upload(file);
		String imagePath = serverURL+file.getOriginalFilename();
		List<String> tockenlist = appusersservices.getSingleTocken(userId);
		String tocken[] = tockenlist.stream().toArray(String[]::new);
		System.out.println("tocken::" + tocken.length);
		PushFCMNotifiction pm = new PushFCMNotifiction();
		String resp = null;
		try {
			resp = pm.adminsendPushNotificationbytopic(tocken,notiTitle, notiMsg, imagePath);
			return new SpringException(true, resp).toString();
		}catch (Exception e) {
			return new SpringException(false, e.getMessage()).toString();
		}
	}
	
	
	@PostMapping(value = "/addCategory",produces = {"application/json"})
//	public String addCategory(@RequestBody CategoryMaster cm) {
	public String addCategory(@RequestParam(value="fileName") MultipartFile file,@RequestParam(value="catName") String catName,
			@RequestParam(value="parentCatId") long parentCatId) {	
		String imagePath = "NA";
		try {
			if(file.isEmpty()) {
				
			}else {
				FTPUploadJob job = new FTPUploadJob();
				job.upload(file);
				imagePath = file.getOriginalFilename();
			}
			CategoryMaster cm = new CategoryMaster();
			cm.setCatName(catName);
			cm.setParentCatId(parentCatId);
			cm.setDocName(imagePath);
			categorymasterservice.saveCategoty(cm);
			return new SpringException(true, "Category Sucessfully Added").toString();
		}catch (DataIntegrityViolationException e) {
			return new SpringException(false, "Duplicate Entry Found").toString();
		}catch (Exception e) {
			e.printStackTrace();
			return new SpringException(false, "Please enter proper details").toString();
		}
	}
	
	@PostMapping(value = "/updateCategoryNew",produces = {"application/json"})
	public String updateCategoryNew(@RequestParam(value="fileName") MultipartFile file,@RequestParam(value="catName") String catName,
			@RequestParam(value="parentCatId") long parentCatId,@RequestParam(value="catId") long childId) {	
		String imagePath = "NA";
		try {
			if(file.isEmpty()) {
				Optional<CategoryMaster> list = categorymasterservice.getCategoryMasterDetailsById(childId);
				imagePath = list.get().getDocName();
			}else {
				FTPUploadJob job = new FTPUploadJob();
				job.upload(file);
				imagePath = file.getOriginalFilename();
			}
			CategoryMaster cm = new CategoryMaster();
			cm.setCatName(catName);
			cm.setParentCatId(parentCatId);
			cm.setDocName(imagePath);
			cm.setCatId(childId);
			categorymasterservice.updateCategoryMaster(cm);
			return new SpringException(true, "Category Sucessfully Added").toString();
		}catch (DataIntegrityViolationException e) {
			return new SpringException(false, "Duplicate Entry Found").toString();
		}catch (Exception e) {
			e.printStackTrace();
			return new SpringException(false, "Please enter proper details").toString();
		}
	}
	
	@GetMapping(value = "/getAllCategoryList", produces = { "application/json" })
	public List<CategoryMaster> getAllCategoryList() throws Exception {
		return categorymasterservice.getCategoryMasterList();
	}
	
	@GetMapping(value = "/getCategoryById/{id}",produces = { "application/json" })
	public Optional<CategoryMaster> getCategoryById(@PathVariable long id) {
		return categorymasterservice.getCategoryMasterDetailsById(id);
	}
	
	@DeleteMapping(value = "/deleteCategory/{id}")
	public String deleteCategory(@PathVariable int id) {
		categorymasterservice.deleteCatByParent(id);
		return categorymasterservice.deleteCategoryMasterById(id);
	}
	
	@PostMapping(value = "/updateCategory", produces = { "application/json" })
	public String updateCategory(@RequestBody CategoryMaster cm) throws Exception {
		try {
			categorymasterservice.updateCategoryMaster(cm);
			return new SpringException(true, "Category Updated Successfully").toString();
		}catch (DataIntegrityViolationException e) {
			return new SpringException(false, "Duplicate Entry Found").toString();
		}catch (Exception e) {
			e.printStackTrace();
			return new SpringException(false, e.getMessage()).toString();
		}
	}
	
	@GetMapping(value = "/getCategoryMasterData" ,produces = { "application/json" })
	public String getCategoryMasterData() throws Exception {
		JSONArray jarray = new JSONArray();
		int counter=1;
		List<Object[]> catMasterList = categorymasterservice.getCategoryMasterData();
		for (Object[] result : catMasterList) {
			JSONObject jobj = new JSONObject();
			jobj.put("childId", result[0]);
			jobj.put("childCatName",  result[1].toString().trim());
			jobj.put("parentCatName", result[2]);
			if(result[3].equals("NA")) {
				jobj.put("docName", "");
				jobj.put("docName1", "");
			}else {
				jobj.put("docName", serverURL+result[3]);
				jobj.put("docName1", result[3]);
			}
			jobj.put("parentId", result[4]);
			jobj.put("autoNo", counter);
			jarray.put(jobj);
			counter++; 
		}
		return jarray.toString();
	}
	
	@GetMapping(value = "/updateDocStat/{id}",produces = { "application/json" })
	public String updateDocStat(@PathVariable long id) {
		categorymasterservice.updateDocStat(id);
		return "Doc Deleted";
	}
	
	@GetMapping(value = "/getDashboardData" ,produces = { "application/json" })
	public String getDashboardData() throws Exception {
		JSONObject jobj = new JSONObject();
		List<Object[]> catMasterList = categorymasterservice.getDashboardData();
		for (Object[] result : catMasterList) {
			jobj.put("userCount", result[0]);
			jobj.put("bannerCount",  result[1]);
			jobj.put("catCount", result[2]);
			jobj.put("couponCount", result[3]);
			jobj.put("videoCount", result[4]);
			jobj.put("itemCount", result[5]);
			jobj.put("itemCountActive", result[6]);
			jobj.put("itemCountDeActive", result[7]);
			jobj.put("couponCountScanned", result[8]);
			jobj.put("couponCountScannRM", result[9]);
			jobj.put("ordersCount", result[10]);
			jobj.put("complainCount", result[11]);
			jobj.put("feedbackCount", result[12]);
		}
		return jobj.toString();
	}
	
	@PostMapping(value = "/addItem",produces = {"application/json"})
	public String addItem(@RequestParam(value="itemPrimaryImg") MultipartFile file,@RequestParam(value="itemImg1") MultipartFile file1,
			@RequestParam(value="itemImg2") MultipartFile file2,@RequestParam(value="itemImg3") MultipartFile file3,
			@RequestParam(value="itemName") String itemName,@RequestParam(value="itemDesc") String itemDesc,
			@RequestParam(value="marketPrice") double marketPrice,@RequestParam(value="offerPrice") double offerPrice,
			@RequestParam(value="minQty") long minQty,@RequestParam(value="maxQty") long maxQty,@RequestParam(value="avalibleQty") long avalibleQty,
			@RequestParam(value="catId") long catId,@RequestParam(value="itemPoints") int itemPoints) {	
		String primaryImagePath = "NA";
		String fileImagePath1 = "NA";
		String fileImagePath2 = "NA";
		String fileImagePath3 = "NA";
		try {
			if(file.isEmpty()) {
			}else {
				FTPUploadJob job = new FTPUploadJob();
				job.upload(file);
				primaryImagePath = file.getOriginalFilename();
			}
			if(file1.isEmpty()) {
			}else {
				FTPUploadJob job = new FTPUploadJob();
				job.upload(file1);
				fileImagePath1 = file1.getOriginalFilename();
			}
			if(file2.isEmpty()) {
			}else {
				FTPUploadJob job = new FTPUploadJob();
				job.upload(file2);
				fileImagePath2 = file2.getOriginalFilename();
			}
			if(file3.isEmpty()) {
			}else {
				FTPUploadJob job = new FTPUploadJob();
				job.upload(file3);
				fileImagePath3 = file3.getOriginalFilename();
			}
			ItemMaster im = new ItemMaster();
			im.setAvalibleQty(avalibleQty);
			im.setItemDesc(itemDesc);
			im.setItemImg1(fileImagePath1);
			im.setItemImg2(fileImagePath2);
			im.setItemImg3(fileImagePath3);
			im.setItemName(itemName);
			im.setItemPrimaryImg(primaryImagePath);
			im.setItemStat(true);
			im.setMarketPrice(marketPrice);
			im.setMaxQty(maxQty);
			im.setMinQty(minQty);
			im.setOfferPrice(offerPrice);
			im.setCatId(catId);
			im.setItemPoints(itemPoints);
			itemmasterservice.addItem(im);
			return new SpringException(true, "Item Sucessfully Added").toString();
		}catch (DataIntegrityViolationException e) {
			return new SpringException(false, "Duplicate Entry Found").toString();
		}catch (Exception e) {
			e.printStackTrace();
			return new SpringException(false, "Please enter proper details").toString();
		}
	}
	
	@GetMapping(value = "/getItemMasterData" ,produces = { "application/json" })
	public String getItemMasterData() throws Exception {
		JSONArray jarray = new JSONArray();
		int counter=1;
		List<Object[]> itemList = appusersservices.getDataByFilter("SELECT im.itemId,im.avalibleQty,im.itemDesc,im.itemImg1,im.itemImg2,im.itemImg3,im.itemName,im.itemPrimaryImg,im.itemStat,\r\n" + 
				"       im.marketPrice,im.maxQty,im.minQty,im.offerPrice,cm.catName,im.catId,im.itemPoints FROM itemmaster im JOIN itemcategory cm ON im.catId=cm.catId ");
		for (Object[] result : itemList) {
			JSONObject jobj = new JSONObject();
			jobj.put("itemId", result[0]);
			jobj.put("availibleQty",  result[1].toString().trim());
			jobj.put("itemDesc", result[2]);
			jobj.put("image1", result[3].toString().equals("NA") ? "NA" : result[3]);
			jobj.put("image2", result[4].toString().equals("NA") ? "NA" : result[4]);
			jobj.put("image3", result[5].toString().equals("NA") ? "NA" : result[5]);
			jobj.put("itemName", result[6]);
			jobj.put("imagePrimary", result[7].toString().equals("NA") ? "NA" : result[7]);
			jobj.put("itemStat", result[8]);
			jobj.put("marketPrice", result[9]);
			jobj.put("maxQty", result[10]);
			jobj.put("minQty", result[11]);
			jobj.put("offerPrice", result[12]);
			jobj.put("categoryName", result[13]);
			jobj.put("categoryId", result[14]);
			jobj.put("itemPoints", result[15]);
			jobj.put("autoNo", counter);
			jarray.put(jobj);
			counter++; 
		}
		return jarray.toString();
	}
	
	@DeleteMapping(value = "/deleteItem/{id}")
	public String deleteItem(@PathVariable long id) {
		return itemmasterservice.deleteItemById(id);
	}
	
	@GetMapping(value = "/changeItemStat/{id}/{itemSatat}")
	public String changeItemStat(@PathVariable long id,@PathVariable boolean itemSatat) {
		int stat = commonservice.updateData("UPDATE itemmaster SET itemStat="+itemSatat+" WHERE itemId="+id+" ");
		if(itemSatat==true) {
			return "Succesfully Activated";
		}else {
			return "Succesfully DeActivated";
		}
	}
	
	@DeleteMapping(value = "/deleteItemDocument/{itemId}/{itemImgNum}")
	public String deleteItemDocument(@PathVariable long itemId,@PathVariable int itemImgNum) {
		StringBuilder sb = new StringBuilder("UPDATE itemmaster");
		if(itemImgNum==0) {
			sb.append(" SET itemPrimaryImg='NA' ");
		}else if(itemImgNum==1) {
			sb.append(" SET itemImg1='NA' ");
		}else if(itemImgNum==2) {
			sb.append(" SET itemImg2='NA' ");
		}else if(itemImgNum==3) {
			sb.append(" SET itemImg3='NA' ");
		}else {
			
		}
		sb.append(" WHERE itemId="+itemId+" ");
		
		int stat = commonservice.updateData(sb.toString());
		return "Succesfully DeActivated";
	}
	
	@PostMapping(value = "/updateItem",produces = {"application/json"})
	public String updateItem(@RequestParam(value="itemPrimaryImg") MultipartFile file,@RequestParam(value="itemImg1") MultipartFile file1,
			@RequestParam(value="itemImg2") MultipartFile file2,@RequestParam(value="itemImg3") MultipartFile file3,
			@RequestParam(value="itemName") String itemName,@RequestParam(value="itemDesc") String itemDesc,
			@RequestParam(value="marketPrice") double marketPrice,@RequestParam(value="offerPrice") double offerPrice,
			@RequestParam(value="minQty") long minQty,@RequestParam(value="maxQty") long maxQty,
			@RequestParam(value="avalibleQty") long avalibleQty,@RequestParam(value="itemId") long itemId,@RequestParam(value="catId") long catId,@RequestParam(value="itemPoints") int itemPoints) {	
		
		String primaryImagePath = "NA";
		String fileImagePath1 = "NA";
		String fileImagePath2 = "NA";
		String fileImagePath3 = "NA";
		try {
			if(file.isEmpty()) {
			}else {
				FTPUploadJob job = new FTPUploadJob();
				job.upload(file);
				primaryImagePath = file.getOriginalFilename();
			}
			if(file1.isEmpty()) {
			}else {
				FTPUploadJob job = new FTPUploadJob();
				job.upload(file1);
				fileImagePath1 = file1.getOriginalFilename();
			}
			if(file2.isEmpty()) {
			}else {
				FTPUploadJob job = new FTPUploadJob();
				job.upload(file2);
				fileImagePath2 = file2.getOriginalFilename();
			}
			if(file3.isEmpty()) {
			}else {
				FTPUploadJob job = new FTPUploadJob();
				job.upload(file3);
				fileImagePath3 = file3.getOriginalFilename();
			}
			ItemMaster im = new ItemMaster();
			im.setAvalibleQty(avalibleQty);
			im.setItemDesc(itemDesc);
			im.setItemImg1(fileImagePath1);
			im.setItemImg2(fileImagePath2);
			im.setItemImg3(fileImagePath3);
			im.setItemName(itemName);
			im.setItemPrimaryImg(primaryImagePath);
			im.setItemStat(true);
			im.setMarketPrice(marketPrice);
			im.setMaxQty(maxQty);
			im.setMinQty(minQty);
			im.setOfferPrice(offerPrice);
			im.setItemId(itemId);
			im.setCatId(catId);
			im.setItemPoints(itemPoints);
			itemmasterservice.updateItem(im);
			return new SpringException(true, "Item Sucessfully Updated").toString();
		}catch (DataIntegrityViolationException e) {
			return new SpringException(false, "Duplicate Entry Found").toString();
		}catch (Exception e) {
			e.printStackTrace();
			return new SpringException(false, "Please enter proper details").toString();
		}
	}
	
	@GetMapping(value = "/getAllOrderList" ,produces = { "application/json" })
	public String getAllOrderList() throws Exception {
		JSONArray jarray = new JSONArray();
		List<Object[]> myTransList = appusersservices.getDataByFilter("SELECT shm.shopingId,shm.city,shm.courierName,im.itemName,shm.itemQty,shm.offerPrice,shm.orderAmt,shm.orderDate,shm.orderStatus,shm.pinCode,\r\n" + 
				"       shm.shippingAddress,shm.trackNum,CONCAT(au.firstName,' ', au.lastName) AS userName,shm.userStatmentId,shm.userId\r\n" + 
				"       FROM shopingmaster shm JOIN itemmaster im  ON shm.itemId=im.itemId JOIN appusers au ON shm.userId=au.userId");
		
		int counter = 1;
		for(Object[] result : myTransList) {
			JSONObject jobj = new JSONObject();
			jobj.put("orderId", result[0]);
			jobj.put("city", result[1]);
			jobj.put("courierName", result[2].toString());
			jobj.put("itemName", result[3].toString());
			jobj.put("itemQty", result[4]);
			jobj.put("offerPrice", result[5]);
			jobj.put("orderAmt", result[6]);
			jobj.put("orderDate", formatter2.format(formatter2.parse(result[7].toString())));
			jobj.put("orderStatus", result[8].toString());
			jobj.put("pinCode", result[9].toString());
			jobj.put("shippingAddress", result[10].toString());
			jobj.put("trackNum", result[11].toString());
			jobj.put("userName", result[12].toString());
			jobj.put("userStatmentId", result[13].toString());
			jobj.put("userId", result[14]);
			jobj.put("autoNo", counter);
			jarray.put(jobj);
			counter++;
		}
		return jarray.toString();
	}
	
	@GetMapping(value = "/changeOrderStatus/{orderId}/{orderStat}")
	public String changeOrderStatus(@PathVariable long orderId,@PathVariable String orderStat) {
		if(orderStat.equals("ACCEPTED")) {
			double orderAmt=0;
			long userId = 0;
			List<Object[]> orderDetailsList = appusersservices.getDataByFilter("SELECT * FROM shopingmaster WHERE shopingId="+orderId+" ");
			for(Object[] result : orderDetailsList) {
				orderAmt = (double) result[6];
				userId =  Long.parseLong(result[12].toString());
			}
			double walletBalance=0;
			List<Object[]> userDetailsList = appusersservices.getDataByFilter("SELECT * FROM appusers WHERE userId="+userId+" ");
			for(Object[] result : userDetailsList) {
				walletBalance = (double) result[12];
			}
			if(orderAmt > walletBalance) {
				return "Your Wallet Balance Is Low";
			}else {
				StringBuilder sb = new StringBuilder("UPDATE shopingmaster SET orderStatus='"+orderStat+"' WHERE shopingId="+orderId+ "");
				int stat = commonservice.updateData(sb.toString());
				double updateWallrtBalance = walletBalance - orderAmt;
				
				UserTransactions utrans = new UserTransactions();
				utrans.setAmount(orderAmt);
				utrans.setBankTransId(0);
				utrans.setCloseAmt(updateWallrtBalance);
				utrans.setOpenAmt(walletBalance);
				utrans.setQrcodeId(0);
				utrans.setShopingOrderId(orderId);
				utrans.setTransDate(new Date());
				utrans.setTransType("Dr");
				utrans.setUserId(userId);
				usertransactionsservice.saveTarnsaction(utrans);
				int stat1 = commonservice.updateData("UPDATE appUsers SET walletBalance="+updateWallrtBalance+" WHERE userId="+userId+" ");
				return "Status Succesfully Changed";
			}
		}else {
			StringBuilder sb = new StringBuilder("UPDATE shopingmaster SET orderStatus='"+orderStat+"' WHERE shopingId="+orderId+ "");
			int stat = commonservice.updateData(sb.toString());
			return "Status Succesfully Changed";
		}
	}
	
	@GetMapping(value = "/getUserTransactionList" ,produces = { "application/json" })
	public String getUserTransactionList() throws Exception {
		JSONArray jarray = new JSONArray();
		List<Object[]> userTransList = appusersservices.getDataByFilter("SELECT ut.trId,ut.amount,ut.openAmt,ut.closeAmt,ut.transType,CONCAT(au.firstName,' ',au.lastName)AS userName,\r\n" + 
				" ut.bankTransId,ut.qrcodeId,ut.shopingOrderId,ut.transDate FROM usertransactions ut JOIN appusers au ON ut.userId= au.userId");
		int counter = 1;
		for(Object[] result : userTransList) {
			JSONObject jobj = new JSONObject();
			jobj.put("trId", result[0]);
			jobj.put("amount", result[1]);
			jobj.put("openAmt", result[2].toString());
			jobj.put("closeAmt", result[3].toString());
			jobj.put("transType", result[4]);
			jobj.put("userName", result[5]);
			jobj.put("bankTransId", result[6]);
		//	jobj.put("qrCodeId", result[7].toString());
			if(Long.parseLong(result[7].toString())==0) {
				jobj.put("qrCodeId", "");
			}else {
				try {
					Optional<CouponMaster> list = couponmasterservices.getCouponDetailsById(Long.parseLong(result[7].toString()));
					jobj.put("qrCodeId", list.get().getCpnCode());
				} catch (NoSuchElementException e) {
					jobj.put("qrCodeId", "");
				}
			}
			jobj.put("shopingOrderId", result[8].toString());
			jobj.put("transDate", formatter2.format(formatter2.parse(result[9].toString())));
			jobj.put("autoNo", counter);
			jarray.put(jobj);
			counter++;
		}
		return jarray.toString();
	}
	
	@GetMapping(value = "/getUserStatement/{userId}" ,produces = { "application/json" })
	public String getUserStatement(@PathVariable("userId") long userId) throws Exception {
		JSONArray jarray = new JSONArray();
		List<Object[]> myTransList = appusersservices.getDataByFilter("SELECT ut.trId,ut.amount,ut.openAmt,ut.closeAmt,ut.transType,CONCAT(au.firstName,' ',au.lastName)AS userName," + 
				                   " ut.bankTransId,ut.qrcodeId,ut.shopingOrderId,ut.transDate FROM usertransactions ut JOIN appusers au ON ut.userId= au.userId WHERE ut.userId="+userId+" ");
		int counter = 1;
		for(Object[] result : myTransList) {
			JSONObject jobj = new JSONObject();
			jobj.put("trId", result[0]);
			jobj.put("amount", result[1]);
			jobj.put("openAmt", result[2].toString());
			jobj.put("closeAmt", result[3].toString());
			jobj.put("transType", result[4]);
			jobj.put("userName", result[5]);
			jobj.put("bankTransId", result[6]);
			jobj.put("qrCodeId", result[7].toString());
			jobj.put("shopingOrderId", result[8].toString());
			jobj.put("transDate", formatter2.format(formatter2.parse(result[9].toString())));
			jobj.put("autoNo", counter);
			jarray.put(jobj);
			counter++;
		}
		return jarray.toString();
	}
	
	@GetMapping(value = "/getUserOrderHistory/{userId}" ,produces = { "application/json" })
	public String getUserOrderHistory(@PathVariable("userId") long userId) throws Exception {
		JSONArray jarray = new JSONArray();
		List<Object[]> userOderHistory = appusersservices.getDataByFilter("SELECT shm.shopingId,im.itemName,shm.itemQty,shm.orderAmt,shm.offerPrice,shm.orderStatus,shm.orderDate,shm.city,shm.shippingAddress,shm.pinCode,\r\n" + 
				"	shm.courierName,shm.trackNum FROM shopingmaster shm JOIN itemmaster im ON shm.itemId=im.itemId WHERE shm.userId="+userId+" ");
		int counter = 1;
		for(Object[] result : userOderHistory) {
			JSONObject jobj = new JSONObject();
			jobj.put("shopingId", result[0]);
			jobj.put("itemName", result[1].toString());
			jobj.put("itemQty", result[2]);
			jobj.put("orderAmt", result[3]);
			jobj.put("offerPrice", result[4]);
			jobj.put("orderStatus", result[5].toString());
			jobj.put("orderDate", formatter2.format(formatter2.parse(result[6].toString())));
			jobj.put("city", result[7].toString());
			jobj.put("shippingAddress", result[8].toString());
			jobj.put("pinCode", result[9].toString());
			jobj.put("courierName", result[10].toString());
			jobj.put("trackNum", result[11].toString());
			jobj.put("autoNo", counter);
			jarray.put(jobj);
			counter++;
		}
		return jarray.toString();
	}
	
	@GetMapping(value = "/getUserQrCodes/{userId}" ,produces = { "application/json" })
	public String getUserQrCodes(@PathVariable("userId") long userId) throws Exception {
		JSONArray jarray = new JSONArray();
		List<Object[]> userOderHistory = appusersservices.getDataByFilter("SELECT qrs.autoNo,CONCAT(au.firstName,' ',au.lastName) AS userName,qrs.addedOn,qrs.codeAmt,qrs.cpnCode \r\n" + 
				"       FROM qrcodescan qrs JOIN appusers au ON qrs.userId=au.userId WHERE qrs.userId="+userId+" ");
		for(Object[] result : userOderHistory) {
			JSONObject jobj = new JSONObject();
			jobj.put("autoNo", result[0]);
			jobj.put("userName", result[1].toString());
			jobj.put("codeAmt", result[3]);
			jobj.put("cpnCode", result[4]);
			jobj.put("addedOn", formatter2.format(formatter2.parse(result[2].toString())));
			jarray.put(jobj);
		}
		return jarray.toString();
	}
	
	@PostMapping(value = "/addImageGallary",produces = {"application/json"})
	public String addImageGallary(@RequestParam(value="images") MultipartFile[] files,@RequestParam(value="imageName") String imageName,
								  @RequestParam(value="imageDesc") String imageDesc) {	
		try {
			
			if(files.length >= 1) {
				Arrays.asList(files).stream().forEach(file -> {
					if(!file.isEmpty()) {
						FTPUploadJob job = new FTPUploadJob();
						try {
							job.upload(file);
						} catch (Exception e) {
							e.printStackTrace();
						}
						ImageGallary ig = new ImageGallary();
						ig.setImageDesc(imageDesc);
						ig.setImageName(imageName);
						ig.setImagePath(file.getOriginalFilename());
						ig.setImageStatus(true);
						ig.setImgAddedon(new Date());
						imagegallaryservices.saveImageGallaryData(ig);
					}
			      });
			}else {
				System.out.println("Empty Files");
				ImageGallary ig = new ImageGallary();
				ig.setImageDesc(imageDesc);
				ig.setImageName(imageName);
				ig.setImagePath("");
				ig.setImageStatus(true);
				ig.setImgAddedon(new Date());
				imagegallaryservices.saveImageGallaryData(ig);
			}
			return new SpringException(true, "Image Sucessfully Added").toString();
		}catch (Exception e) {
			e.printStackTrace();
			return new SpringException(false, "Please enter proper details").toString();
		}
	}
	
	@GetMapping(value = "/getImageGallery" ,produces = { "application/json" })
	public String getImageGallery() throws Exception {
		JSONArray jarray = new JSONArray();
		List<Object[]> igList = appusersservices.getDataByFilter("SELECT * FROM imagegallary GROUP BY imageName");
		int counter=1;
		for(Object[] result : igList) {
			JSONObject jobj = new JSONObject();
			jobj.put("igId", result[0]);
			jobj.put("imgDesc", result[1].toString().trim());
			jobj.put("imageName", result[2]);
		//	jobj.put("imagePath", result[3]);
			jobj.put("imageStatus", result[4]);
			jobj.put("addedOn", formatter2.format(formatter2.parse(result[5].toString())));
			jobj.put("autoNo", counter);
			jarray.put(jobj);
			counter++;
		}
		return jarray.toString();
	}
	
	@GetMapping(value = "/getImagesPath/{imgName}" ,produces = { "application/json" })
	public String getImagesPath(@PathVariable String imgName) throws Exception {
		JSONArray jarray = new JSONArray();
		List<Object[]> igList = appusersservices.getDataByFilter("SELECT * FROM imagegallary WHERE imageName='"+imgName+"' ");
		int counter=1;
		for(Object[] result : igList) {
			JSONObject jobj = new JSONObject();
			jobj.put("igId", result[0]);
			jobj.put("imgDesc", result[1].toString().trim());
			jobj.put("imageName", result[2]);
			jobj.put("imagePath", result[3]);
			jobj.put("imageStatus", result[4]);
			jobj.put("addedOn", formatter2.format(formatter2.parse(result[5].toString())));
			jobj.put("autoNo", counter);
			jarray.put(jobj);
			counter++;
		}
		return jarray.toString();
	}
	
	@GetMapping(value = "/delImage/{imgPath}" ,produces = { "application/json" })
	public String delImage(@PathVariable String imgPath) throws Exception {
		int stat = commonservice.updateData("DELETE FROM imagegallary WHERE imagePath='"+imgPath+"' ");
		return "DELETED";
	}
	
	@GetMapping(value = "/getFeedbacks" ,produces = { "application/json" })
	public String getFeedbacks() throws Exception {
		JSONArray jarray = new JSONArray();
		List<Object[]> fdList = appusersservices.getDataByFilter("SELECT fd.fdId,fd.addedon,fd.fdMessage,fd.fdStatus,CONCAT(au.firstName,' ',au.lastName) AS userName \r\n" + 
											"       FROM feedback fd JOIN appusers au ON fd.userId=au.userId");
		int counter=1;
		for(Object[] result : fdList) {
			JSONObject jobj = new JSONObject();
			jobj.put("fdId", result[0]);
			jobj.put("addedOn", formatter2.format(formatter2.parse(result[1].toString())));
			jobj.put("fdMessage", result[2].toString().trim());
			jobj.put("fdStatus", result[3]);
			jobj.put("userName", result[4]);
			jobj.put("autoNo", counter);
			jarray.put(jobj);
			counter++;
		}
		return jarray.toString();
	}
	
	@DeleteMapping(value = "/delFeedback/{fdId}")
	public String delFeedback(@PathVariable long fdId) throws Exception {
		feedbackservices.deleteFeedback(fdId);
		return "DELETED";
	}
	
	@GetMapping(value = "/changeFDStatus/{fdId}")
	public String changeFDStatus(@PathVariable long fdId) throws Exception {
		int stat = commonservice.updateData("UPDATE feedback SET fdStatus='READ' WHERE fdId="+fdId+" ");
		return "UPDATED";
	}
	
	@GetMapping(value = "/getComplains" ,produces = { "application/json" })
	public String getComplains() throws Exception {
		JSONArray jarray = new JSONArray();
		List<Object[]> cmpList = appusersservices.getDataByFilter("SELECT cp.cpId,cp.addedon,cp.adminMsg,cp.cpMessage,cp.cpStatus,CONCAT(au.firstName,' ',au.lastName) AS userName \r\n" + 
											"       FROM complain cp JOIN appusers au ON cp.userId=au.userId");
		int counter=1;
		for(Object[] result : cmpList) {
			JSONObject jobj = new JSONObject();
			jobj.put("cpId", result[0]);
			jobj.put("addedOn", formatter2.format(formatter2.parse(result[1].toString())));
			jobj.put("adminMessage", result[2].toString().trim());
			jobj.put("cpMessage", result[3].toString().trim());
			jobj.put("cpStatus", result[4]);
			jobj.put("userName", result[5]);
			jobj.put("autoNo", counter);
			jarray.put(jobj);
			counter++;
		}
		return jarray.toString();
	}
	
	@DeleteMapping(value = "/delComplain/{cpId}")
	public String delComplain(@PathVariable long cpId) throws Exception {
		complainservices.deleteComplain(cpId);
		return "DELETED";
	}
	
	@GetMapping(value = "/replyComplain/{cpId}/{adminResp}")
	public String replyComplain(@PathVariable long cpId,@PathVariable String adminResp) throws Exception {
		int stat = commonservice.updateData("UPDATE complain SET adminMsg='"+adminResp+"',cpStatus='REPLIED' WHERE cpId="+cpId+" ");
		return "UPDATED";
	}
	
	@PostMapping(value = "/editUser",produces = {"application/json"})
	public String editUser(@RequestParam("firstName") String firstName,@RequestParam("lastName") String lastName,@RequestParam("mobileNo") String mobileNo,
			@RequestParam("waMobileNo") String waMobileNo,@RequestParam("userType") String userType,@RequestParam("address") String address,
			@RequestParam("state") String state,@RequestParam("city") String city,@RequestParam("walletBalance") double walletBalance,
			@RequestParam("userId") long userId) {
		try {
			Optional<AppUsers> au1 = appusersservices.getUserDetailsById(userId);
			
			AppUsers au = new AppUsers();
			au.setAddress(address);
			au.setCity(city);
			au.setFirstName(firstName);
			au.setLastName(lastName);
			au.setMobileNo(mobileNo);
			au.setState(state);
			au.setUserType(userType);
			au.setWalletBalance(walletBalance);
			au.setWaMobileNo(waMobileNo);
			au.setUserId(userId);
			au.setFcmid(au1.get().getFcmid());
			au.setImei(au1.get().getImei());
			au.setDateOfBirth(au1.get().getDateOfBirth());
			appusersservices.updateAppUsers(au);
			return new SpringException(true, "User Sucessfully Updated").toString();
		}catch (DataIntegrityViolationException e) {
			return new SpringException(false, "User Already Register With this Mobile Number").toString();
		}catch (Exception e) {
			e.printStackTrace();
			return new SpringException(false, "Please enter proper details").toString();
		}
	}
	
	@PostMapping(value = "/editOrder",produces = { "application/json" })
	public String editOrder(@RequestParam("shopingId") long shopingId,@RequestParam("itemQty") long itemQty,@RequestParam("offerPrice") double offerPrice,
							@RequestParam("orderAmt") double orderAmt,@RequestParam("shippingAddress") String shippingAddress,@RequestParam("city") String city,
							@RequestParam("pinCode") String pinCode,@RequestParam("courierName") String courierName,@RequestParam("trackNum") String trackNum) throws Exception {
		long itemId = 0,userId = 0,userstatId = 0;
		String orderStatus = "";
		Date orderdate = new Date();
		try {
			List<Object[]> orderList = appusersservices.getDataByFilter("SELECT * FROM shopingmaster WHERE shopingId="+shopingId+" ");
			for(Object[] result : orderList) {
				itemId= Long.valueOf(result[3].toString());
				userId=Long.valueOf(result[12].toString());
				userstatId=Long.valueOf(result[13].toString());
				orderStatus = result[8].toString();
				orderdate = (Date) result[7];
			}
			
			ShopingMaster shm = new ShopingMaster();
			shm.setCity(city);
			shm.setCourierName("");
			shm.setItemId(itemId);
			shm.setItemQty(itemQty);
			shm.setOfferPrice(offerPrice);
			shm.setOrderAmt(orderAmt);
			shm.setOrderDate(orderdate);
			shm.setOrderStatus(orderStatus);
			shm.setPinCode(pinCode);
			shm.setShippingAddress(shippingAddress);
			shm.setTrackNum(trackNum);
			shm.setCourierName(courierName);
			shm.setUserId(userId);
			shm.setUserStatmentId(userstatId);
			shm.setShopingId(shopingId);
			shopingmasterservices.updateOrder(shm);
			return new SpringException(true, "ORDER UPDATED").toString();
		} catch (Exception e) {
			e.printStackTrace();
			return new SpringException(false, "Something Went Wrong").toString();
		}
	}
	
/*	@GetMapping(value = "/getAllCouponExcelExport", produces = { "application/json" })
	public @ResponseBody ModelAndView getAllCouponExcelExport() throws Exception {
		List<Object[]> dataList = appusersservices.getDataByFilter("SELECT cm.cpnId,cm.cpnCode,cm.cpnName WHERE cm.isUsed=FALSE ORDER BY addedOn DESC");

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("excelList", dataList);
		return new ModelAndView(new LeadDataExcelBuilder(), model);
	}*/
	
	@PostMapping(value = "/saveExtraDetails",consumes = {"application/json"})
	public String saveExtraDetails(@RequestBody ExtraDetails ed) {
		try {
			extradetailsservices.saveExtraDetails(ed);
			return new SpringException(true, "Sucessfully Added").toString();
		}catch (DataIntegrityViolationException e) {
			return new SpringException(false, "Content Already Added").toString();
		}catch (Exception e) {
			e.printStackTrace();
			return new SpringException(false, "Please enter proper details").toString();
		}
	}
	
	@GetMapping(value = "/getExtraDetails" ,produces = { "application/json" })
	public String getExtraDetails() throws Exception {
		JSONArray jarray = new JSONArray();
		List<Object[]> cmpList = appusersservices.getDataByFilter("SELECT * FROM extradetails");
		int counter=1;
		for(Object[] result : cmpList) {
			JSONObject jobj = new JSONObject();
			jobj.put("edId", result[0]);
			jobj.put("edParam", result[2].toString().trim());
			jobj.put("edDesc", result[1].toString().trim());
			jobj.put("autoNo", counter);
			jarray.put(jobj);
			counter++;
		}
		return jarray.toString();
	}
	
	@DeleteMapping(value = "/delExtraDetails/{cpId}")
	public String delExtraDetails(@PathVariable long cpId) throws Exception {
		return extradetailsservices.deleteDataById(cpId);
	}
	
	@PostMapping(value = "/updateExtraDetails",consumes = {"application/json"})
	public String updateExtraDetails(@RequestBody ExtraDetails ed) {
		try {
			extradetailsservices.updateExtraDetails(ed);
			return new SpringException(true, "Sucessfully Added").toString();
		}catch (DataIntegrityViolationException e) {
			return new SpringException(false, "Content Already Added").toString();
		}catch (Exception e) {
			e.printStackTrace();
			return new SpringException(false, "Please enter proper details").toString();
		}
	}
	
	@PostMapping(value = "/addItemCategory",produces = {"application/json"})
	public String addItemCategory(@RequestParam(value="catName") String catName,@RequestParam(value="parentCatId") long parentCatId) {	
		try {
			ItemCategory cm = new ItemCategory();
			cm.setCatName(catName);
			cm.setParentCatId(parentCatId);
			itemcategoryservices.saveItemCategory(cm);
			return new SpringException(true, "Category Sucessfully Added").toString();
		}catch (DataIntegrityViolationException e) {
			return new SpringException(false, "Duplicate Entry Found").toString();
		}catch (Exception e) {
			e.printStackTrace();
			return new SpringException(false, "Please enter proper details").toString();
		}
	}
	
	@GetMapping(value = "/getAllItemCategoryList", produces = { "application/json" })
	public List<ItemCategory> getAllItemCategoryList() throws Exception {
		return itemcategoryservices.getAllItemCategory();
	}
	
	@GetMapping(value = "/getItemCategoryData" ,produces = { "application/json" })
	public String getItemCategoryData() throws Exception {
		JSONArray jarray = new JSONArray();
		int counter=1;
		List<Object[]> dataList = appusersservices.getDataByFilter("SELECT b.catId AS childId,b.catName AS childCat,a.catName AS parentCat,a.catId AS parentId FROM itemcategory a JOIN itemcategory b ON a.catId=b.parentCatId");
		for (Object[] result : dataList) {
			JSONObject jobj = new JSONObject();
			jobj.put("childId", result[0]);
			jobj.put("childCatName",  result[1].toString().trim());
			jobj.put("parentCatName", result[2]);
			jobj.put("parentId", result[3]);
			jobj.put("autoNo", counter);
			jarray.put(jobj);
			counter++; 
		}
		return jarray.toString();
	}
	
	@PostMapping(value = "/updateItemCategory",produces = {"application/json"})
	public String updateItemCategory(@RequestParam(value="catName") String catName,@RequestParam(value="parentCatId") long parentCatId,
			                       @RequestParam(value="catId") long childId) {	
		try {
			ItemCategory cm = new ItemCategory();
			cm.setCatName(catName);
			cm.setParentCatId(parentCatId);
			cm.setCatId(childId);
			itemcategoryservices.updateItemCategory(cm);
			return new SpringException(true, "Category Sucessfully Changed").toString();
		}catch (DataIntegrityViolationException e) {
			return new SpringException(false, "Duplicate Entry Found").toString();
		}catch (Exception e) {
			e.printStackTrace();
			return new SpringException(false, "Please enter proper details").toString();
		}
	}
	
	@DeleteMapping(value = "/delItemCategory/{cpId}")
	public String delItemCategory(@PathVariable long cpId) throws Exception {
		 itemcategoryservices.deleteItemCat(cpId);
		 return "DELETED";
	}
	
	@GetMapping(value = "/updateAdminPass/{pass}")
	public String updateAdminPass(@PathVariable("pass") String password) throws Exception {
		appusersservices.updateAdminPassword("admin", EncrytedPasswordUtils.encrytePassword(password));
        return "Changed";
	}
	
	
	@GetMapping(value = "/delCouponsByRange/{startRng}/{endRng}")
	public String delCouponsByRange(@PathVariable String startRng,@PathVariable String endRng) throws Exception {
		try {
			if(startRng.equals("0") && endRng.equals("0")) {
				couponmasterservices.deleteCouponMsterAll();
				return new SpringException(true, "Requested Data Deleted").toString();
			}else {
				List<Object[]> dataList = appusersservices.getDataByFilter("SELECT * FROM couponmaster WHERE cpnUniqNum BETWEEN '"+startRng+"' AND '"+endRng+"'");
				if(dataList.size()==0) {
					return new SpringException(false, "No Data Found").toString();
				}
				 couponmasterservices.deleteCouponMsterByRange(startRng, endRng);
				return new SpringException(true, "Requested Data Deleted").toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new SpringException(false, "Exception Arise").toString();
		}
		
	}
}
