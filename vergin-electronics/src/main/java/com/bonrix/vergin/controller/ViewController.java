package com.bonrix.vergin.controller;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

@CrossOrigin("*")
@Controller
public class ViewController {
	
	@GetMapping(value = "/home")
    public String home(Map<String, Object> model) {
        return "index";
    }
	
	@GetMapping(value = "/header")
    public String header(Map<String, Object> model) {
        return "Header";
    }
	
	@GetMapping(value = "/headerInfo")
    public String headerInfo(Map<String, Object> model) {
        return "HeaderInfo";
    }
	
	@GetMapping(value = "/androidUser")
    public String androidUser(Map<String, Object> model) {
        return "pages/AndroidUsers";
    }
	
	@GetMapping(value = "/couponMasterNew")
    public String couponMaster(Map<String, Object> model) {
        return "pages/CouponMaster";
    }
	
	@GetMapping(value = "/couponMaster")
    public String couponMasterNew(Map<String, Object> model) {
        return "pages/CouponMasterNew";
    }
	
	@GetMapping(value = "/BannerManage")
    public String BannerManage(Map<String, Object> model) {
        return "pages/BannerManage";
    }
	
	@GetMapping(value = "/videoGallery")
    public String videoGallery(Map<String, Object> model) {
        return "pages/VideoGallary";
    }
	
	@GetMapping(value = "/smsSetting")
    public String smsSetting(Map<String, Object> model) {
        return "pages/SMSSetting";
    }
	
	@GetMapping(value = "/smsTemplate")
    public String smsTemplate(Map<String, Object> model) {
        return "pages/SMSTemplate";
    }
	
	@GetMapping(value = "/smsLog")
    public String smsLog(Map<String, Object> model) {
        return "pages/SMSLog";
    }
	
	@GetMapping(value = "/notification")
    public String notification(Map<String, Object> model) {
        return "pages/Notification";
    }
	
	@GetMapping(value = "/productPortfolio")
    public String categoryManage(Map<String, Object> model) {
        return "pages/CategoryManage";
    }
	
	@GetMapping(value = "/ItemMaster")
    public String ItemMasterPage(Map<String, Object> model) {
        return "pages/ItemMaster";
    }
	
	@GetMapping(value = "/orderManage")
    public String orderManage(Map<String, Object> model) {
        return "pages/OrderManage";
    }
	
	@GetMapping(value = "/userTransAdm")
    public String userTransAdm(Map<String, Object> model) {
        return "pages/UserTransactions";
    }
	
	@GetMapping(value = "/imageGallary")
    public String imageGallary(Map<String, Object> model) {
        return "pages/ImageGallary";
    }
	
	@GetMapping(value = "/feedbackManage")
    public String feedbackManage(Map<String, Object> model) {
        return "pages/FeedbackManage";
    }
	
	@GetMapping(value = "/complainManage")
    public String complainManage(Map<String, Object> model) {
        return "pages/ComplainManage";
    }
	
	@GetMapping(value = "/ExtraDetails")
    public String ExtraDetails(Map<String, Object> model) {
        return "pages/ExtraDetails";
    }
	
	@GetMapping(value = "/ItemCategory")
    public String ItemCategory(Map<String, Object> model) {
        return "pages/ItemCategory";
    }
	
	@GetMapping(value = "/changeAdminPass")
    public String changeAdminPass(Map<String, Object> model) {
        return "pages/AdminChangePass";
    }
	

	
}