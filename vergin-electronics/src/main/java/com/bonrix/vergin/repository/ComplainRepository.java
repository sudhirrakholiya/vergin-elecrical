package com.bonrix.vergin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bonrix.vergin.model.Complain;

public interface ComplainRepository extends JpaRepository<Complain, Long>{

}
