package com.bonrix.vergin.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bonrix.vergin.model.CouponMaster;

public interface CouponMasterRepository extends JpaRepository<CouponMaster, Long>{
	
	
    @Query(value="SELECT cm.cpnId,cm.addedOn,cm.cpnCode,cm.cpnName,cm.cpnUsedOn,cm.isUsed,cm.usedByUid,cm.cpnPrice,\r\n" + 
     		"    COALESCE(CONCAT(au.firstName,' ', au.lastName), '')  AS userName,cm.cpnUniqNum FROM couponmaster cm  LEFT JOIN appusers au ON cm.usedByUid= au.userId ORDER BY addedOn DESC",nativeQuery=true)
	List<Object[]> getCouponListData();
	
	@Modifying
	@Transactional
	@Query(value="UPDATE couponmaster SET isUsed=TRUE,usedByUid=:uid,cpnUsedOn=:usedt WHERE cpnCode=:couponCode" ,nativeQuery=true)
	void updateCouponUse(@Param("couponCode") String couponCode,@Param("uid") long uid,@Param("usedt") Date usedt);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE couponmaster SET isUsed=TRUE,usedByUid=:uid,cpnUsedOn=:usedt WHERE cpnId=:cpnId" ,nativeQuery=true)
	void updateCouponUseAdmin(@Param("cpnId") long cpnId,@Param("uid") long uid,@Param("usedt") Date usedt);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE user SET LastUnNum=:lsatUNQNum WHERE Username='admin' " ,nativeQuery=true)
	void updateCouponLastUnqNum(@Param("lsatUNQNum") String lsatUNQNum);
	
	
	@Modifying
	@Transactional
	@Query(value="DELETE FROM couponmaster WHERE cpnUniqNum BETWEEN :startRng AND :endRng ",nativeQuery=true)
	void deleteCouponMsterByRange(@Param("startRng") String startRng,@Param("endRng") String endRng);

}
