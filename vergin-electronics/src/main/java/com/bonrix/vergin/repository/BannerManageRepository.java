package com.bonrix.vergin.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bonrix.vergin.model.BannerManage;

public interface BannerManageRepository extends JpaRepository<BannerManage, Long>{
	
	
	@Query(value="SELECT * FROM bannermanage ORDER BY bnrAddedOn DESC",nativeQuery=true)
	List<Object[]> getBannerListData();
	
	@Modifying
	@Transactional
	@Query(value="UPDATE bannermanage SET bnrStatus=:bnrStatus WHERE bnrId=:bnrId",nativeQuery=true)
	void updateBannerStat(@Param("bnrStatus") boolean bnrStatus,@Param("bnrId") long bnrId);

}
