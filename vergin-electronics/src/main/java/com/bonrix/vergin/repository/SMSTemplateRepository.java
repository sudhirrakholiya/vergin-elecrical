package com.bonrix.vergin.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bonrix.vergin.model.SMSTemplate;

public interface SMSTemplateRepository extends JpaRepository<SMSTemplate, Long>{
	
	@Query(value = "SELECT * FROM smstemplate",nativeQuery = true)
	List<Object[]> smstemplateListData();
	
	@Modifying
	@Transactional
	@Query(value="UPDATE smstemplate SET tempStatus=:stat WHERE tempId=:tempId",nativeQuery=true)
	void updatesmstempStat(@Param("stat") boolean stat,@Param("tempId") long tempId);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE smstemplate SET tempStatus=FALSE WHERE tempId!=:tempId",nativeQuery=true)
	void updatesmstempOther(@Param("tempId") long tempId);

}
