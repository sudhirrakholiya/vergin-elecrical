package com.bonrix.vergin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bonrix.vergin.model.QRCodeScan;

public interface QRCodeScanRepository extends JpaRepository<QRCodeScan, Long>{

}
