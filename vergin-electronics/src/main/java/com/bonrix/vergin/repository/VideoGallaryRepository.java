package com.bonrix.vergin.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bonrix.vergin.model.VideoGallary;

public interface VideoGallaryRepository extends JpaRepository<VideoGallary, Long>{

	@Query(value="SELECT * FROM videogallary ORDER BY videoAddedOn DESC",nativeQuery=true)
	List<Object[]> getVideoGallaryListData();
	
	@Modifying
	@Transactional
	@Query(value="UPDATE videogallary SET videoStatus=:vidStatus WHERE vgId=:vidId",nativeQuery=true)
	void updateVideoStat(@Param("vidStatus") boolean vidStatus,@Param("vidId") long vidId);
}
