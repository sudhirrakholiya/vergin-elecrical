package com.bonrix.vergin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bonrix.vergin.model.ExtraDetails;

public interface ExtraDetailsRepository extends JpaRepository<ExtraDetails, Long>{

}
