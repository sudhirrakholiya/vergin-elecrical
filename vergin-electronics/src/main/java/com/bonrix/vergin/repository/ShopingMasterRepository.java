package com.bonrix.vergin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bonrix.vergin.model.ShopingMaster;

public interface ShopingMasterRepository extends JpaRepository<ShopingMaster, Long>{

}
