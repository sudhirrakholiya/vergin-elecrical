package com.bonrix.vergin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bonrix.vergin.model.ImageGallary;

public interface ImageGallaryRepository extends JpaRepository<ImageGallary, Long>{

}
