package com.bonrix.vergin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bonrix.vergin.model.ItemCategory;

public interface ItemCategoryRepository extends JpaRepository<ItemCategory, Long>{

}
