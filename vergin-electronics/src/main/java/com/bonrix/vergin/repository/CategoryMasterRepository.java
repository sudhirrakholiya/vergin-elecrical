package com.bonrix.vergin.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bonrix.vergin.model.CategoryMaster;

public interface CategoryMasterRepository extends JpaRepository<CategoryMaster, Long>{
	
	
	@Query(value = "SELECT b.catId AS childId,b.catName AS childCat,a.catName AS parentCat,b.docName,a.catId AS parentId FROM categorymaster a JOIN categorymaster b ON a.catId=b.parentCatId\r\n" + 
			"  ",nativeQuery = true)
	List<Object[]> getCategoryMasterData();
	
	@Modifying
	@Transactional
	@Query(value="UPDATE categorymaster SET docName='NA' WHERE catId=:catId",nativeQuery=true)
	void updateDocStat(@Param("catId") long catId);
	
	@Query(value = "SELECT b.catId AS childId,b.catName AS childCat,a.catName AS parentCat,b.docName,a.catId AS parentId FROM categorymaster a JOIN categorymaster b ON a.catId=b.parentCatId  WHERE a.catId=:parentCatId  \r\n" + 
			"  ",nativeQuery = true)
	List<Object[]> getCategoryMasterDataByParentId(@Param("parentCatId") long parentCatId);
	
	@Modifying
	@Transactional
	@Query(value="DELETE FROM categorymaster WHERE parentCatId=:catId",nativeQuery=true)
	void deleteCatByParent(@Param("catId") long catId);
	
	
	@Query(value = "SELECT (SELECT COUNT(*) FROM appusers) AS userCoun,(SELECT COUNT(*) FROM bannermanage) AS bannerCount,\r\n" + 
			"       (SELECT COUNT(*) FROM categorymaster) AS catCount,(SELECT COUNT(*) FROM couponmaster) AS couponCount,\r\n" + 
			"       (SELECT COUNT(*) FROM videogallary) AS videoCount,(SELECT COUNT(*) FROM itemmaster) AS itemCount,\r\n" + 
			"       (SELECT COUNT(*) FROM itemmaster WHERE itemStat=TRUE) AS itemCountActive,\r\n" + 
			"       (SELECT COUNT(*) FROM itemmaster WHERE itemStat=FALSE) AS itemCountDeActive,"+
		    "		(SELECT COUNT(*) FROM couponmaster WHERE isUsed=TRUE) AS couponCountScanned,\r\n" + 
			"       (SELECT COUNT(*) FROM couponmaster WHERE isUsed=FALSE) AS couponCountScannedRM,(SELECT COUNT(*) FROM shopingmaster) AS orderCount,"+
			"       (SELECT COUNT(*) FROM complain) AS complainCount,(SELECT COUNT(*) FROM feedback) AS feedbackCount",nativeQuery = true)
	List<Object[]> getDashboardData();

}
