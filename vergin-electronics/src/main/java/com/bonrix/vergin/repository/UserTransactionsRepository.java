package com.bonrix.vergin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bonrix.vergin.model.UserTransactions;

public interface UserTransactionsRepository extends JpaRepository<UserTransactions, Long>{

}
