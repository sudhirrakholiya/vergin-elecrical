package com.bonrix.vergin.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.bonrix.vergin.model.SentSMSLog;

public interface SentSMSLogRepository extends JpaRepository<SentSMSLog, Long>{
	
	@Query(value = "SELECT * FROM sentsmslog ORDER BY sendDate DESC",nativeQuery = true)
	List<Object[]> getSMSLogList();

}
