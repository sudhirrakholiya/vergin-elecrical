package com.bonrix.vergin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bonrix.vergin.model.Feedback;

public interface FeedbackRepository extends JpaRepository<Feedback, Long>{

}
