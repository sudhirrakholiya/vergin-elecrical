package com.bonrix.vergin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bonrix.vergin.model.ItemMaster;

public interface ItemMasterRepository extends JpaRepository<ItemMaster, Long>{

}
