package com.bonrix.vergin.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bonrix.vergin.model.AppUsers;

public interface AppUsersRepository extends JpaRepository<AppUsers, Long>{
	
	@Query(value = "SELECT fcmid FROM appusers WHERE fcmid!=''",nativeQuery = true)
	public List<String> gettocken();
	
	@Query(value = "SELECT fcmid FROM appusers WHERE userId=:userId ",nativeQuery = true)
	public List<String> getSingleTocken(long userId);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE user SET Password=:password WHERE Username=:userName",nativeQuery=true)
	void updateAdminPassword(@Param("userName") String userName,@Param("password") String password);
	
}
