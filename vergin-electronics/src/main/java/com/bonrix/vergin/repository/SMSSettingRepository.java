package com.bonrix.vergin.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bonrix.vergin.model.SMSSetting;

public interface SMSSettingRepository extends JpaRepository<SMSSetting, Long>{
	
	@Query(value = "SELECT * FROM smssetting",nativeQuery = true)
	List<Object[]> settingListData();
	
	@Modifying
	@Transactional
	@Query(value="UPDATE smssetting SET settStatus=:stat WHERE setId=:setId",nativeQuery=true)
	void updateSMSSettStat(@Param("stat") boolean stat,@Param("setId") long setId);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE smssetting SET settStatus=FALSE WHERE setId!=:setId",nativeQuery=true)
	void updateSMSSettStatOther(@Param("setId") long setId);

}
