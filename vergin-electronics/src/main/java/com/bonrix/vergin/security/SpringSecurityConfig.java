package com.bonrix.vergin.security;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;
    
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
    	 http
         .csrf().disable()
         .authorizeRequests()
         .antMatchers("/apis/**").permitAll()
         .antMatchers("/bower_components/**").permitAll()
         .antMatchers("/dist/**").permitAll()
         .antMatchers("/plugins/**").permitAll()
         .antMatchers("/image/**").permitAll()
         .antMatchers("/js/**").permitAll()
         .antMatchers("/css/**").permitAll()
         .antMatchers("/login*").permitAll()
         .anyRequest().authenticated()
         .and()
         .formLogin()
         .loginPage("/login.html")
         .loginProcessingUrl("/login")
         .defaultSuccessUrl("/home", true)
         .failureUrl("/loginError.html?error=true")
         //.failureHandler(authenticationFailureHandler())
         .and()
         .logout()
         .logoutUrl("/perform_logout")
         .deleteCookies("JSESSIONID");
         //.logoutSuccessHandler(logoutSuccessHandler());
    }

    @Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/*.css");
		web.ignoring().antMatchers("/*.js");
		web.ignoring().antMatchers("/*.img");
		web.ignoring().antMatchers("/*.jpg");
		web.ignoring().antMatchers("/*.png");
		web.ignoring().antMatchers("/v2/api-docs",
                "/configuration/ui", "/swagger-resources/**",
                "/configuration/security", "/swagger-ui.html","/webjars/**");
    }
    
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
               .authoritiesByUsernameQuery("select Username, Role from user where Username=?")
                .usersByUsernameQuery("select Username, Password, 1 as enabled from user where Username=?");
    }
    
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}