package com.bonrix.vergin.android;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.bonrix.vergin.job.FTPUploadJob;
import com.bonrix.vergin.model.AppUsers;
import com.bonrix.vergin.model.BannerManage;
import com.bonrix.vergin.model.Complain;
import com.bonrix.vergin.model.CouponMaster;
import com.bonrix.vergin.model.Feedback;
import com.bonrix.vergin.model.QRCodeScan;
import com.bonrix.vergin.model.SentSMSLog;
import com.bonrix.vergin.model.ShopingMaster;
import com.bonrix.vergin.model.SpringException;
import com.bonrix.vergin.model.UserTransactions;
import com.bonrix.vergin.model.VideoGallary;
import com.bonrix.vergin.services.AppUsersServices;
import com.bonrix.vergin.services.BannerManageServices;
import com.bonrix.vergin.services.CategoryMasterService;
import com.bonrix.vergin.services.CommonService;
import com.bonrix.vergin.services.ComplainServices;
import com.bonrix.vergin.services.CouponMasterServices;
import com.bonrix.vergin.services.ExtraDetailsServices;
import com.bonrix.vergin.services.FeedbackServices;
import com.bonrix.vergin.services.QRCodeScanServices;
import com.bonrix.vergin.services.SMSSettingService;
import com.bonrix.vergin.services.SMSTemplateService;
import com.bonrix.vergin.services.SentSMSLogService;
import com.bonrix.vergin.services.ShopingMasterServices;
import com.bonrix.vergin.services.UserTransactionsService;
import com.bonrix.vergin.services.VideoGallaryServices;

import lombok.extern.log4j.Log4j2;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/apis")
@Log4j2
public class AndroidController {
	
	@Autowired
	AppUsersServices appusersservices;
	
	@Autowired
	CouponMasterServices couponmasterservices;
	
	@Autowired
	SentSMSLogService sentsmslogservice;
	
	@Autowired
	CommonService commonservice;
	
	@Autowired
	BannerManageServices bannermanageservices;
	
	@Autowired
	VideoGallaryServices videogallaryservices;
	
	@Autowired
	CategoryMasterService categorymasterservice;
	
	@Autowired
	UserTransactionsService usertransactionsservice;
	
	@Autowired
	ShopingMasterServices shopingmasterservices;
	
	@Autowired
	QRCodeScanServices qrcodescanservices;
	
	@Autowired
	FeedbackServices feedbackservices;
	
	@Autowired
	ComplainServices complainservices;
	
	@Autowired
	SMSSettingService smssettingservice;
	
	@Autowired
	SMSTemplateService smstemplateservice;
	
	@Autowired
	ExtraDetailsServices extradetailsservices;
	
	
	SimpleDateFormat formatter1=new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat formatter2=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat formatter3=new SimpleDateFormat("dd-MM-yyyy");
	
	static String serverURL = "http://159.89.175.120/UploadFile/vergin/";
	String httpResponse = "";
	
	Logger log = LoggerFactory.getLogger(AndroidController.class);
	
	@PostMapping(value = "/registerUser",consumes = {"application/json"})
	public String registerUser(@RequestBody AppUsers au) {
		try {
			log.info("Test Log");
			System.out.println("PreData::"+ au.getDateOfBirth());
			System.out.println("Date::"+formatter3.format(formatter3.parse(au.getDateOfBirth())));   //formatter1.parse(au.getDateOfBirth())
			au.setDateOfBirth(formatter3.format(formatter3.parse(au.getDateOfBirth())));
			appusersservices.registerUser(au);
			return new SpringException(true, "User Sucessfully Added").toString();
		}catch (DataIntegrityViolationException e) {
			return new SpringException(false, "User already registered with this mobile number").toString();
		}catch (Exception e) {
			e.printStackTrace();
			return new SpringException(false, "Please enter proper details").toString();
		}
	}
	
	/*
	 * @GetMapping(value = "/getAllUserList", produces = { "application/json" })
	 * public List<AppUsers> getAllUserList() throws Exception { return
	 * appusersservices.getUserList(); }
	 */
	
	@GetMapping(value = "/getUserById",produces = { "application/json" })
	public String getUserById(@RequestParam("id") long id) throws JSONException, ParseException {
		JSONObject jobjMain = new JSONObject();
		JSONArray jarray = new JSONArray();
		AppUsers us = (AppUsers) commonservice.getSingleObject("FROM AppUsers WHERE userId="+id+" ");
		if (us == null) {
			jobjMain.put("status", false);
			jobjMain.put("message", "This mobile number is not registered, Please register first");
			jobjMain.put("Data", jarray);
			return jobjMain.toString();
		}else {
			String query = "SELECT * FROM appusers WHERE userId="+id+" ";
			List<Object[]> userList = appusersservices.getDataByFilter(query);
			for(Object[] result : userList) {
				JSONObject jobjInner = new JSONObject();
				jobjInner.put("userId", result[0].toString());
				jobjInner.put("firstName", result[1]==null ? "" : result[1].toString());
				jobjInner.put("lastName", result[2]==null ? "" : result[2].toString());
				jobjInner.put("dateOfBirth", result[3].toString());
				jobjInner.put("mobileNo", result[4]==null ? "" : result[4].toString());
				jobjInner.put("userType", result[5]==null ? "" : result[5].toString());
				jobjInner.put("whatsappMobile", result[6]==null ? "" : result[6].toString());
				jobjInner.put("address", result[7]==null ? "" : result[7].toString());
				jobjInner.put("city", result[8]==null ? "" : result[8].toString());
				jobjInner.put("state", result[9]==null ? "" : result[9].toString());
				jobjInner.put("walletBalance", result[12]==null ? "" : result[12].toString());
				jarray.put(jobjInner);
			}
			jobjMain.put("status", true);
			jobjMain.put("message", "User Data By Id");
			jobjMain.put("Data", jarray);
			return jobjMain.toString();
		}
	}
	
	@PostMapping(value = "/updateUserDetails",consumes = {"application/json"})
	public String updateUserDetails(@RequestBody AppUsers au) {
		try {
			AppUsers us = (AppUsers) commonservice.getSingleObject("FROM AppUsers WHERE userId="+au.getUserId()+" ");
			au.setWalletBalance(us.getWalletBalance());
			au.setUserId(au.getUserId());
			appusersservices.updateAppUsers(au);
			return new SpringException(true, "User sucessfully updated").toString();
		}catch (DataIntegrityViolationException e) {
			return new SpringException(false, "User already registered with this mobile number").toString();
		}catch (Exception e) {
			e.printStackTrace();
			return new SpringException(false, "Please enter proper details").toString();
		}
	}
	
	
	@GetMapping(value = "/androidLogin",produces = { "application/json" })
	public String androidLogin(@RequestParam("mobileNo") String mobileNo)throws UnsupportedEncodingException {
		JSONObject jobj = new JSONObject();
		JSONArray jarray = new JSONArray();
		log.info("Hello Login");
		try {
			AppUsers userCount = (AppUsers) commonservice.getSingleObject("FROM AppUsers WHERE mobileNo="+mobileNo+" ");
			if(userCount==null) {
				jobj.put("status", false);
				jobj.put("message", "This mobile number is not registered, Please register first");
				jobj.put("Data", jarray);
				return jobj.toString();
			}else {
				int value = 1;
				int otp = (int) (Math.random() * 9000.0D) + 1000;
	//			String msg = otp+" "+"is Your OTP For Login Into Vergin Electronics.";
    //			String url = "http://topsms.bonrix.in//sendsms.aspx?mobile=9909820485&pass=verin543&senderid=VERGIN&to=<mobile_number>&msg=<message>";
			
				String msg = "";
				List<Object[]> smstempList = smstemplateservice.SMSTemplateListData();
				for (Object[] result : smstempList) {
					msg = result[1].toString().trim();
				}
				msg = msg.replaceAll("\\<OTP\\>", String.valueOf(otp));
				msg = URLEncoder.encode(msg, "UTF-8");
				
				String url = "";
				List<Object[]> smssettList = smssettingservice.settingListData();
				for (Object[] result : smssettList) {
					url = result[1].toString().trim();
				}
				url = url.replaceAll("\\<mobile_number\\>", mobileNo.trim());
				url = url.replaceAll("\\<message\\>", msg.trim());
				//System.out.println("COMPLETE URL" + url);

				int n = 0;
				String smsURL = "";
				String token = "";
				try {
				  URL sms = new URL(url);
					HttpURLConnection httpConn = (HttpURLConnection) sms.openConnection();
					BufferedReader httpReader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
					this.httpResponse = "";
					String line = "";
					while ((line = httpReader.readLine()) != null) {
						this.httpResponse = line;
				//		System.out.println("Conformation From SMS Provider: :" + this.httpResponse);
						if (this.httpResponse.equalsIgnoreCase("Message dropped but Not Sent".trim())) {
							value = 0;
						}
					} 
					httpReader.close();
					httpConn.disconnect();
					
					SentSMSLog sl = new SentSMSLog();
					sl.setAPIResponse("");
					sl.setMessage(msg.replace("+", " "));
					sl.setMessageStatus("SUCCESS");
					sl.setMobileNo(mobileNo);
					sl.setSendDate(new Date());
					sentsmslogservice.saveSMSLog(sl);
				} catch (Exception e) {
					e.printStackTrace();
					SentSMSLog sl = new SentSMSLog();
					sl.setAPIResponse("");
					sl.setMessage(msg.replace("+", " "));
					sl.setMessageStatus("FAIL");
					sl.setMobileNo(mobileNo);
					sl.setSendDate(new Date());
					sentsmslogservice.saveSMSLog(sl);
				}
				long userId = userCount.getUserId();
				String query = "SELECT * FROM appusers WHERE userId="+userId+" ";
				List<Object[]> userList = appusersservices.getDataByFilter(query);
				for(Object[] result : userList) {
					JSONObject jobjInner = new JSONObject();
					jobjInner.put("userId", result[0].toString());
					jobjInner.put("firstName", result[1].toString());
					jobjInner.put("lastName", result[2].toString());
					jobjInner.put("dateOfBirth", result[3].toString());
					jobjInner.put("mobileNo", result[4].toString());
					jobjInner.put("userType", result[5].toString());
					jobjInner.put("whatsappMobile", result[6].toString());
					jobjInner.put("address", result[7].toString());
					jobjInner.put("city", result[8].toString());
					jobjInner.put("state", result[9].toString());
					jarray.put(jobjInner);
				}
				jobj.put("status",true);
				jobj.put("message", "User Details By Mobile Number");
				jobj.put("OTP", otp);
				jobj.put("Data", jarray);
				return jobj.toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			jobj.put("status",true);
			jobj.put("message", e.getMessage());
			jobj.put("Data", jarray);
			return jobj.toString();
		}
	}
	
/*  @GetMapping(value = "/getAllCouponListData", produces = { "application/json" })
	public String getAllCouponListData() throws Exception {
		JSONObject jobjMain = new JSONObject();
		JSONArray jarray = new JSONArray();
		List<Object[]> dataList = couponmasterservices.getCouponListData();
		for(Object[] result : dataList) {
			JSONObject jobj = new JSONObject();
			jobj.put("cpnId", result[0]);
			jobj.put("addedOn",  formatter2.format(formatter2.parse(result[1].toString())));
			jobj.put("cpnCode", result[2]);
			jobj.put("cpnName", result[3]);
			jobj.put("cpnUsedOn", formatter1.format(formatter1.parse(result[4].toString())));
			jobj.put("IsUsed", result[5]);
			jobj.put("usedByUid", result[6]);
			jobj.put("cpnPrice", result[7]);
			jobj.put("usedBy", result[8]);
			jarray.put(jobj);
		}
		jobjMain.put("status", true);
		jobjMain.put("message", "All Coupon List");
		jobjMain.put("Data", jarray);
		return jobjMain.toString();
	}*/
	
	@GetMapping(value = "/getCouponListById",produces = { "application/json" })
	public String getCouponListById(@RequestParam("id") long id) {
		JSONObject jobjMain = new JSONObject();
		JSONArray jarray = new JSONArray();
		CouponMaster us = (CouponMaster) commonservice.getSingleObject("FROM CouponMaster WHERE cpnId="+id+" ");
		if (us == null) {
			jobjMain.put("status", false);
			jobjMain.put("message", "No coupon found");
			jobjMain.put("Data", jarray);
			return jobjMain.toString();
		}else {
			String query = "SELECT cm.cpnId,cm.addedOn,cm.cpnCode,cm.cpnName,cm.cpnUsedOn,cm.isUsed,CONCAT(au.firstName,\" \", au.lastName )AS usedByUser,\r\n" + 
							"cm.cpnPrice FROM couponmaster cm JOIN appusers au ON  cm.usedByUid=au.userId WHERE  cm.cpnId="+id+" ";
			List<Object[]> userList = appusersservices.getDataByFilter(query);
			for(Object[] result : userList) {
				JSONObject jobjInner = new JSONObject();
				jobjInner.put("cpnId", result[0].toString());
				jobjInner.put("addedOn", result[1].toString());
				jobjInner.put("cpnCode", result[2].toString());
				jobjInner.put("cpnName", result[3].toString());
				jobjInner.put("cpnUsedOn", result[4].toString());
				jobjInner.put("isUsed", result[5].toString());
				jobjInner.put("usedBy", result[6].toString());
				jobjInner.put("cpnPrice", result[7].toString());
				jarray.put(jobjInner);
			}
			jobjMain.put("status", true);
			jobjMain.put("message", "Coupon Data By Coupon Id");
			jobjMain.put("Data", jarray);
			return jobjMain.toString();
		}
	}
	
	@GetMapping(value = "/scanCouponCode",produces = {"application/json"})
	public String scanCouponCode(@RequestParam("couponCode") String couponCode,@RequestParam("userId") long userId) {
		JSONObject jobjMain = new JSONObject();
		JSONArray jarray = new JSONArray();
		AppUsers us1 = (AppUsers) commonservice.getSingleObject("FROM AppUsers WHERE userId="+userId+" ");
		if (us1 == null) {
			jobjMain.put("status", false);
			jobjMain.put("message", "This mobile number is not registered, Please register first");
			jobjMain.put("Data", jarray);
			return jobjMain.toString();
		}
		CouponMaster us = (CouponMaster) commonservice.getSingleObject("FROM CouponMaster WHERE cpnCode='"+couponCode+"' ");
		if (us == null) {
			jobjMain.put("status", false);
			jobjMain.put("message", "No coupon found");
			jobjMain.put("Data", jarray);
			return jobjMain.toString();
		}else {
			CouponMaster us2 = (CouponMaster) commonservice.getSingleObject("FROM CouponMaster WHERE cpnCode='"+couponCode+"' AND isUsed=TRUE");
			if (!(us2 == null)) {
				jobjMain.put("status", false);
				jobjMain.put("message", "Invalid coupon code");
				jobjMain.put("Data", jarray);
				return jobjMain.toString();
			}
			try {
				Date useddt = new Date();
				couponmasterservices.updateCouponUse(couponCode,userId,useddt);
				int stat = commonservice.updateData("UPDATE appusers SET walletBalance=walletBalance + "+us.getCpnPrice()+" WHERE userId="+userId+" ");
				
				UserTransactions utrans = new UserTransactions();
				utrans.setAmount(us.getCpnPrice());
				utrans.setBankTransId(0);
				utrans.setCloseAmt(us1.getWalletBalance()+us.getCpnPrice());
				utrans.setOpenAmt(us1.getWalletBalance());
				utrans.setQrcodeId(us.getCpnId());
				utrans.setShopingOrderId(0);
				utrans.setTransDate(new Date());
				utrans.setTransType("Cr");
				utrans.setUserId(userId);
				usertransactionsservice.saveTarnsaction(utrans);
				
				QRCodeScan qrs = new QRCodeScan();
				qrs.setCodeAmt(us.getCpnPrice());
				qrs.setCpnCode(us.getCpnCode());
				qrs.setUserId(userId);
				qrs.setAddedOn(new Date());
				qrcodescanservices.saveqQrCodeScanData(qrs);
				
				jobjMain.put("status", true);
				jobjMain.put("message", "Coupon scan successfully");
				jobjMain.put("Data", jarray);
				return jobjMain.toString();
			}catch (Exception e) {
				e.printStackTrace();
				jobjMain.put("status", false);
				jobjMain.put("message", "Please check details");
				jobjMain.put("Data", jarray);
				return jobjMain.toString();
			}
		}
		
	}
	
	@GetMapping(value = "/getAllBannerListData", produces = { "application/json" })
	public String getAllBannerListData() throws Exception {
		JSONObject jobjMain = new JSONObject();
		JSONArray jarray = new JSONArray();
		log.warn("Hello Banner");
		List<Object[]> dataList = bannermanageservices.getBannerListData();
		for(Object[] result : dataList) {
			JSONObject jobj = new JSONObject();
			jobj.put("bnrId", result[0]);
			jobj.put("addedOn",  formatter2.format(formatter2.parse(result[1].toString())));
			jobj.put("bnrName", result[2]);
			jobj.put("bnrStatus", result[3]);
			jobj.put("bnrImage", serverURL+result[4]);
			jarray.put(jobj);
		}
		jobjMain.put("status", true);
		jobjMain.put("message", "All Banner List");
		jobjMain.put("Data", jarray);
		return jobjMain.toString();
	}
	
	@GetMapping(value = "/getBannerListById",produces = { "application/json" })
	public String getBannerListById(@RequestParam("id") long id) throws JSONException, ParseException {
		JSONObject jobjMain = new JSONObject();
		JSONArray jarray = new JSONArray();
		BannerManage us = (BannerManage) commonservice.getSingleObject("FROM BannerManage WHERE bnrId="+id+" ");
		if (us == null) {
			jobjMain.put("status", false);
			jobjMain.put("message", "No banner found");
			jobjMain.put("Data", jarray);
			return jobjMain.toString();
		}else {
			String query = "SELECT * FROM bannermanage WHERE bnrId="+id+" ";
			List<Object[]> userList = appusersservices.getDataByFilter(query);
			for(Object[] result : userList) {
				JSONObject jobjInner = new JSONObject();
				jobjInner.put("bnrId", result[0]);
				jobjInner.put("addedOn",  formatter2.format(formatter2.parse(result[1].toString())));
				jobjInner.put("bnrName", result[2]);
				jobjInner.put("bnrStatus", result[3]);
				jobjInner.put("bnrImage", serverURL+result[4]);
				jarray.put(jobjInner);
			}
			jobjMain.put("status", true);
			jobjMain.put("message", "Banner Data By Banner Id");
			jobjMain.put("Data", jarray);
			return jobjMain.toString();
		}
	}
	
	@GetMapping(value = "/getAllVideoGallaryData", produces = { "application/json" })
	public String getAllVideoGallaryData() throws Exception {
		JSONObject jobjMain = new JSONObject();
		JSONArray jarray = new JSONArray();
		List<Object[]> dataList = videogallaryservices.getVideoGallaryListData();
		for(Object[] result : dataList) {
			JSONObject jobj = new JSONObject();
			jobj.put("videoId", result[0]);
			jobj.put("addedOn",  formatter2.format(formatter2.parse(result[1].toString())));
			jobj.put("videoName", result[2]);
			jobj.put("videoStatus", result[3]);
			jobj.put("videoURL", result[4]);
			jarray.put(jobj);
		}
		jobjMain.put("status", true);
		jobjMain.put("message", "All Video List");
		jobjMain.put("Data", jarray);
		return jobjMain.toString();
	}
	
	@GetMapping(value = "/getVideoById",produces = { "application/json" })
	public String getVideoById(@RequestParam("id") long id) throws JSONException, ParseException {
		JSONObject jobjMain = new JSONObject();
		JSONArray jarray = new JSONArray();
		VideoGallary us = (VideoGallary) commonservice.getSingleObject("FROM VideoGallary WHERE vgId="+id+" ");
		if (us == null) {
			jobjMain.put("status", false);
			jobjMain.put("message", "No video found");
			jobjMain.put("Data", jarray);
			return jobjMain.toString();
		}else {
			String query = "SELECT * FROM videogallary WHERE vgId="+id+" ";
			List<Object[]> userList = appusersservices.getDataByFilter(query);
			for(Object[] result : userList) {
				JSONObject jobjInner = new JSONObject();
				jobjInner.put("videoId", result[0]);
				jobjInner.put("addedOn",  formatter2.format(formatter2.parse(result[1].toString())));
				jobjInner.put("videoName", result[2]);
				jobjInner.put("videoStatus", result[3]);
				jobjInner.put("videoURL", result[4]);
				jarray.put(jobjInner);
			}
			jobjMain.put("status", true);
			jobjMain.put("message", "VideoGallary data by video id");
			jobjMain.put("Data", jarray);
			return jobjMain.toString();
		}
	}
	
/*  @GetMapping(value = "/getCategoryMasterData" ,produces = { "application/json" })
	public String getCategoryMasterData() throws Exception {
		JSONObject jobjmain = new JSONObject();
		JSONArray jarray = new JSONArray();
		int counter=1;
		List<Object[]> catMasterList = categorymasterservice.getCategoryMasterData();
		for (Object[] result : catMasterList) {
			JSONObject jobj = new JSONObject();
			jobj.put("childId", result[0]);
			jobj.put("childCatName",  result[1].toString().trim());
			jobj.put("parentCatName", result[2]);
			if(result[3].equals("NA")) {
				jobj.put("docName", "");
			}else {
				jobj.put("docName", serverURL+result[3]);
			}
			jobj.put("parentId", result[4]);
			jobj.put("autoNo", counter);
			jarray.put(jobj);
			counter++; 
		}
		jobjmain.put("status", true);
		jobjmain.put("message",  "All Category List");
		jobjmain.put("Data", jarray);
		return jobjmain.toString();
	}*/
	
	@GetMapping(value = "/getCategoryMasterData" ,produces = { "application/json" })
	public String getCategoryMasterData() throws Exception {
		JSONObject jobjmain = new JSONObject();
		JSONArray jarray = new JSONArray();
		int counter=1;
		List<Object[]> catMasterList = categorymasterservice.getCategoryMasterData();
		for (Object[] result : catMasterList) {
			JSONObject jobj = new JSONObject();
			jobj.put("childId", result[0]);
			jobj.put("childCatName",  result[1].toString().trim());
			jobj.put("parentCatName", result[2]);
			if(result[3].equals("NA")) {
				jobj.put("docName", "");
			}else {
				jobj.put("docName", serverURL+result[3]);
			}
			jobj.put("parentId", result[4]);
			jobj.put("autoNo", counter);
			
			if(Integer.parseInt(result[4].toString()) == 0) {
				jarray.put(jobj);
				counter++; 
			}
		}
		jobjmain.put("status", true);
		jobjmain.put("message",  "All category list");
		jobjmain.put("Data", jarray);
		return jobjmain.toString();
	}
	
	@GetMapping(value = "/getCategoryMasterDataByParentId" ,produces = { "application/json" })
	public String getCategoryMasterDataByParentId(@RequestParam("parentCatId") long parentCatId) throws Exception {
		JSONObject jobjmain = new JSONObject();
		JSONArray jarray = new JSONArray();
		int counter=1;
		List<Object[]> catMasterList = categorymasterservice.getCategoryMasterDataByParentId(parentCatId);
		for (Object[] result : catMasterList) {
			JSONObject jobj = new JSONObject();
			jobj.put("childId", result[0]);
			jobj.put("childCatName",  result[1].toString().trim());
			jobj.put("parentCatName", result[2]);
			if(result[3].equals("NA")) { 
				jobj.put("docName", "");
			}else {
				jobj.put("docName", serverURL+result[3]);
			}
			jobj.put("parentId", result[4]);
			jobj.put("autoNo", counter);
			jarray.put(jobj);
			counter++; 
		}
		jobjmain.put("status", true);
		jobjmain.put("message",  "Category list by parent");
		jobjmain.put("Data", jarray);
		return jobjmain.toString();
	}
	
	@GetMapping(value = "/getMyWalletBalance" ,produces = { "application/json" })
	public String getMyWalletBalance(@RequestParam("userId") long userId) throws Exception {
		JSONObject jobjmain = new JSONObject();
		JSONArray jarray = new JSONArray();
		AppUsers us = (AppUsers) commonservice.getSingleObject("FROM AppUsers WHERE userId="+userId+" ");
		if (us == null) {
			jobjmain.put("status", false);
			jobjmain.put("message", "This mobile number is not registered, Please register first");
			jobjmain.put("Data", jarray);
			return jobjmain.toString();
		}
		List<Object[]> myBalanceList = appusersservices.getDataByFilter("SELECT walletBalance FROM appusers WHERE userId="+userId+" ");
		System.out.println(myBalanceList.get(0));
		jobjmain.put("status", true);
		jobjmain.put("message",  "My Wallet Balance");
		jobjmain.put("WalletBalance", myBalanceList.get(0));
		jobjmain.put("Data", jarray);
		return jobjmain.toString();
	}
	
	@GetMapping(value = "/getMyScannedCodeList" ,produces = { "application/json" })
	public String getMyScannedCodeList(@RequestParam("userId") long userId) throws Exception {
		JSONObject jobjmain = new JSONObject();
		JSONArray jarray = new JSONArray();
		AppUsers us = (AppUsers) commonservice.getSingleObject("FROM AppUsers WHERE userId="+userId+" ");
		if (us == null) {
			jobjmain.put("status", false);
			jobjmain.put("message", "This mobile number is not registered, Please register first");
			jobjmain.put("Data", jarray);
			return jobjmain.toString();
		}
		List<Object[]> myQrList = appusersservices.getDataByFilter("SELECT * FROM couponmaster WHERE usedByUid="+userId+" ORDER BY cpnUsedOn DESC");
		for(Object[] result : myQrList) {
			JSONObject jobj = new JSONObject();
			jobj.put("cpnId", result[0]);
			jobj.put("addedDate", formatter2.format(formatter2.parse(result[1].toString())));
			jobj.put("cpnCode", result[2].toString());
			jobj.put("cpnName", result[3].toString());
			jobj.put("cpnUsedOn", formatter2.format(formatter2.parse(result[4].toString())));
			jobj.put("cpnAmount", result[7].toString());
			jarray.put(jobj);
		}
		jobjmain.put("status", true);
		jobjmain.put("message",  "My QR Code List");
		jobjmain.put("Data", jarray);
		return jobjmain.toString();
	}
	
	@GetMapping(value = "/getItemMasterData" ,produces = { "application/json" })
	public String getItemMasterData(@RequestParam("catId") long catId) throws Exception {
		JSONObject jobjmain = new JSONObject();
		JSONArray jarray = new JSONArray();
		List<Object[]> itemList = appusersservices.getDataByFilter("SELECT * FROM itemmaster WHERE catId="+catId+" ");
		for (Object[] result : itemList) {
			JSONObject jobj = new JSONObject();
			jobj.put("itemId", result[0]);
			jobj.put("availibleQty",  result[1].toString().trim());
			jobj.put("itemDesc", result[2]);
			jobj.put("image1", result[3].toString().equals("NA") ? "NA" : serverURL+result[3]);
			jobj.put("image2", result[3].toString().equals("NA") ? "NA" : serverURL+result[4]);
			jobj.put("image3", result[3].toString().equals("NA") ? "NA" : serverURL+result[5]);
			jobj.put("itemName", result[6]);
			jobj.put("imagePrimary", result[3].toString().equals("NA") ? "NA" : serverURL+result[7]);
			jobj.put("itemStat", result[8]);
			jobj.put("marketPrice", result[9]);
			jobj.put("maxQty", result[10]);
			jobj.put("minQty", result[11]);
			jobj.put("offerPrice", result[12]);
			jarray.put(jobj);
		}
		jobjmain.put("status", true);
		jobjmain.put("message",  "Item List By Category");
		jobjmain.put("Data", jarray);
		return jobjmain.toString();
	}
	
	@GetMapping(value = "/getMyStatementList" ,produces = { "application/json" })
	public String getMyStatementList(@RequestParam("userId") long userId) throws Exception {
		JSONObject jobjmain = new JSONObject();
		JSONArray jarray = new JSONArray();
		AppUsers us = (AppUsers) commonservice.getSingleObject("FROM AppUsers WHERE userId="+userId+" ");
		if (us == null) {
			jobjmain.put("status", false);
			jobjmain.put("message", "This mobile number is not registered, Please register first");
			jobjmain.put("Data", jarray);
			return jobjmain.toString();
		}
		List<Object[]> myTransList = appusersservices.getDataByFilter("SELECT * FROM usertransactions WHERE userId="+userId+" ORDER BY transDate DESC");
		for(Object[] result : myTransList) {
			JSONObject jobj = new JSONObject();
			jobj.put("trId", result[0]);
			jobj.put("amount", result[1]);
			jobj.put("openAmount", result[2].toString());
			jobj.put("transType", result[3].toString());
			jobj.put("closeAmount", result[5].toString());
			jobj.put("bankTransId", result[6].toString());
			jobj.put("qrCodeId", result[7].toString());
			jobj.put("shopingOrderId", result[8].toString());
			jobj.put("transDate", formatter2.format(formatter2.parse(result[9].toString())));
			jarray.put(jobj);
		}
		jobjmain.put("status", true);
		jobjmain.put("message",  "My Statement List");
		jobjmain.put("Data", jarray);
		return jobjmain.toString();
	}
	
	@PostMapping(value = "/submitOrder" ,produces = { "application/json" })
	public String submitOrder(@RequestParam("userId") long userId,@RequestParam("itemId") long itemId,@RequestParam("itemQty") long itemQty,
							  @RequestParam("offerPrice") double offerPrice,@RequestParam("orderAmt") double orderAmt,
							  @RequestParam("shippingAddress") String shippingAddress,@RequestParam("city") String city,
							  @RequestParam("pinCode") String pinCode) throws Exception {
		JSONObject jobjmain = new JSONObject();
		JSONArray jarray = new JSONArray();
		AppUsers us = (AppUsers) commonservice.getSingleObject("FROM AppUsers WHERE userId="+userId+" ");
		if (us == null) {
			jobjmain.put("status", false);
			jobjmain.put("message", "This mobile number is not registered, Please register first");
			jobjmain.put("Data", jarray);
			return jobjmain.toString();
		}
		
	/*	int walletBal = (int)us.getWalletBalance();
		if(walletBal < (int)orderAmt){
			jobjmain.put("status", false);
			jobjmain.put("message", "You don't have sufficient points");
			jobjmain.put("Data", jarray);
			return jobjmain.toString();
		}*/
		
		ShopingMaster shm = new ShopingMaster();
		shm.setCity(city);
		shm.setCourierName("");
		shm.setItemId(itemId);
		shm.setItemQty(itemQty);
		shm.setOfferPrice(offerPrice);
		shm.setOrderAmt(orderAmt);
		shm.setOrderDate(new Date());
		shm.setOrderStatus("ACCEPTED");
		shm.setPinCode(pinCode);
		shm.setShippingAddress(shippingAddress);
		shm.setTrackNum("");
		shm.setUserId(userId);
		shm.setUserStatmentId(0);
		shopingmasterservices.saveOrder(shm);
		
		int updateWallrtBalance = ((int)us.getWalletBalance() - (int)orderAmt);
		
		UserTransactions utrans = new UserTransactions();
		utrans.setAmount(orderAmt);
		utrans.setBankTransId(0);
		utrans.setCloseAmt(updateWallrtBalance);
		utrans.setOpenAmt(us.getWalletBalance());
		utrans.setQrcodeId(0);
		utrans.setShopingOrderId(shm.getShopingId());
		utrans.setTransDate(new Date());
		utrans.setTransType("Dr");
		utrans.setUserId(userId);
		usertransactionsservice.saveTarnsaction(utrans);
		int stat1 = commonservice.updateData("UPDATE appusers SET walletBalance="+updateWallrtBalance+" WHERE userId="+userId+" ");
		
		JSONObject jobjorder = new JSONObject();
		jobjorder.put("orderId", shm.getShopingId());
		jobjorder.put("city", city);
		jobjorder.put("itemQty", itemQty);
		jobjorder.put("offerPrice", offerPrice);
		jobjorder.put("orderAmt", orderAmt);
		jobjorder.put("orderStatus", shm.getOrderStatus());
		jobjorder.put("pinCode", pinCode);
		jobjorder.put("shippingAddress", shippingAddress);
		jarray.put(jobjorder);
		
		jobjmain.put("status", true);
		jobjmain.put("message",  "Order success");
		jobjmain.put("Data", jarray);
		return jobjmain.toString();
	}
	
	@GetMapping(value = "/getMyOrderList" ,produces = { "application/json" })
	public String getMyOrderList(@RequestParam("userId") long userId) throws Exception {
		JSONObject jobjmain = new JSONObject();
		JSONArray jarray = new JSONArray();
		AppUsers us = (AppUsers) commonservice.getSingleObject("FROM AppUsers WHERE userId="+userId+" ");
		if (us == null) {
			jobjmain.put("status", false);
			jobjmain.put("message", "This mobile number is not registered, Please register first");
			jobjmain.put("Data", jarray);
			return jobjmain.toString();
		}
		//List<Object[]> myTransList = appusersservices.getDataByFilter("SELECT * FROM shopingmaster WHERE userId="+userId+" ");
		List<Object[]> myTransList = appusersservices.getDataByFilter("SELECT sm.shopingId,sm.city,sm.courierName,sm.itemId,sm.itemQty,sm.offerPrice,sm.orderAmt,sm.orderDate,sm.orderStatus,sm.pinCode,\r\n" + 
				"       sm.shippingAddress,sm.trackNum,sm.userId,sm.userStatmentId,im.itemPoints FROM shopingmaster sm JOIN itemmaster im ON sm.itemId=im.itemId WHERE userId="+userId+" ORDER BY sm.orderDate DESC");
		for(Object[] result : myTransList) {
			JSONObject jobj = new JSONObject();
			jobj.put("orderId", result[0]);
			jobj.put("city", result[1]);
			jobj.put("courierName", result[2].toString());
			jobj.put("itemId", result[3].toString());
			jobj.put("itemQty", result[4].toString());
			jobj.put("offerPrice", result[5].toString());
			jobj.put("orderAmt", result[6].toString());
			jobj.put("orderDate", formatter2.format(formatter2.parse(result[7].toString())));
			jobj.put("orderStatus", result[8].toString());
			jobj.put("pinCode", result[9].toString());
			jobj.put("shippingAddress", result[10].toString());
			jobj.put("trackNum", result[11].toString());
			jobj.put("itemPoints", result[14].toString());
			jarray.put(jobj);
		}
		jobjmain.put("status", true);
		jobjmain.put("message",  "My order list");
		jobjmain.put("points",  us.getWalletBalance());
		jobjmain.put("Data", jarray);
		return jobjmain.toString();
	}
	
	@PostMapping(value = "/addFeedback",produces = {"application/json"})
	public String addFeedback(@RequestParam("userId") long userId, @RequestParam("feedbackMsg") String message) {
		JSONObject jobjmain = new JSONObject();
		JSONArray jarray = new JSONArray();
		try {
			AppUsers us = (AppUsers) commonservice.getSingleObject("FROM AppUsers WHERE userId="+userId+" ");
			if (us == null) {
				jobjmain.put("status", false);
				jobjmain.put("message", "This mobile number is not registered, Please register first");
				jobjmain.put("Data", jarray);
				return jobjmain.toString();
			}
			Feedback fd = new Feedback();
			fd.setUserId(userId);
			fd.setFdMessage(message);
			fd.setFdStatus("NEW");
			fd.setAddedon(new Date());
			feedbackservices.saveFeedback(fd);
			jobjmain.put("status", true);
			jobjmain.put("message", "Feedback added");
			jobjmain.put("Data", jarray);
			return jobjmain.toString();
		}catch (Exception e) {
			e.printStackTrace();
			jobjmain.put("status", false);
			jobjmain.put("message", e.getMessage());
			jobjmain.put("Data", jarray);
			return jobjmain.toString();
		}
	}
	
	@PostMapping(value = "/addComplain/{userId}/{complainMessage}",produces = {"application/json"},consumes = {"multipart/form-data"})
	public String addComplain(@PathVariable(value="userId") long userId, @PathVariable(value="complainMessage") String message,
			@RequestParam(value="voiceFile") MultipartFile voiceFile,@RequestParam(value="videoFile") MultipartFile videoFile) {
		JSONObject jobjmain = new JSONObject();
		JSONArray jarray = new JSONArray();
		try {
			AppUsers us = (AppUsers) commonservice.getSingleObject("FROM AppUsers WHERE userId="+userId+" ");
			if (us == null) {
				jobjmain.put("status", false);
				jobjmain.put("message", "This mobile number is not registered, Please register first");
				jobjmain.put("Data", jarray);
				return jobjmain.toString();
			}
			String voicePath = "NA";
			String videoPath = "NA";
			try {
				if(voiceFile.isEmpty()) {
				}else {
					FTPUploadJob job = new FTPUploadJob();
					job.upload(voiceFile);
					voicePath = voiceFile.getOriginalFilename();
				}
				if(videoFile.isEmpty()) {
				}else {
					FTPUploadJob job = new FTPUploadJob();
					job.upload(videoFile);
					videoPath = videoFile.getOriginalFilename();
				}
			}catch (Exception e) {
			}
			
			Complain cp = new Complain();
			cp.setUserId(userId);
			cp.setCpMessage(message);
			cp.setAddedon(new Date());
			cp.setAdminMsg("");
			cp.setCpStatus("PENDING");
			cp.setVideoName(videoPath);
			cp.setVoiceName(voicePath);
			complainservices.addComplain(cp);
			
			jobjmain.put("status", true);
			jobjmain.put("message", "Complain added");
			jobjmain.put("Data", jarray);
			return jobjmain.toString();
		}catch (Exception e) {
			e.printStackTrace();
			jobjmain.put("status", false);
			jobjmain.put("message", e.getMessage());
			jobjmain.put("Data", jarray);
			return jobjmain.toString();
		}
	}
	
	@GetMapping(value = "/getMyComplainsList" ,produces = { "application/json" })
	public String getMyComplainsList(@RequestParam("userId") long userId) throws Exception {
		JSONObject jobjmain = new JSONObject();
		JSONArray jarray = new JSONArray();
		AppUsers us = (AppUsers) commonservice.getSingleObject("FROM AppUsers WHERE userId="+userId+" ");
		if (us == null) {
			jobjmain.put("status", false);
			jobjmain.put("message", "This mobile number is not registered, Please register first");
			jobjmain.put("Data", jarray);
			return jobjmain.toString();
		}
		List<Object[]> myTransList = appusersservices.getDataByFilter("SELECT * FROM complain WHERE userId="+userId+" ");
		for(Object[] result : myTransList) {
			JSONObject jobj = new JSONObject();
			jobj.put("complainId", result[0]);
			jobj.put("complainDate", formatter2.format(formatter2.parse(result[1].toString())));
			jobj.put("adminReplyMessage", result[2].toString().trim());
			jobj.put("complainMessage", result[3].toString().trim());
			jobj.put("complainStatus", result[4]);
			jobj.put("complainVideo", result[6].equals("NA") ? "" : serverURL+result[6]);
			jobj.put("complainVoice", result[7].equals("NA") ? "" : serverURL+result[7]);
			jarray.put(jobj);
		}
		jobjmain.put("status", true);
		jobjmain.put("message",  "My complain list");
		jobjmain.put("Data", jarray);
		return jobjmain.toString();
	}
	
	@GetMapping(value = "/getMyFeedbackList" ,produces = { "application/json" })
	public String getMyFeedbackList(@RequestParam("userId") long userId) throws Exception {
		JSONObject jobjmain = new JSONObject();
		JSONArray jarray = new JSONArray();
		AppUsers us = (AppUsers) commonservice.getSingleObject("FROM AppUsers WHERE userId="+userId+" ");
		if (us == null) {
			jobjmain.put("status", false);
			jobjmain.put("message", "This mobile number is not registered, Please register first");
			jobjmain.put("Data", jarray);
			return jobjmain.toString();
		}
		List<Object[]> myTransList = appusersservices.getDataByFilter("SELECT * FROM feedback WHERE userId="+userId+" ");
		for(Object[] result : myTransList) {
			JSONObject jobj = new JSONObject();
			jobj.put("feedbackId", result[0]);
			jobj.put("feedbackDate", formatter2.format(formatter2.parse(result[1].toString())));
			jobj.put("feedbackMessage", result[2].toString().trim());
			jobj.put("feedbackStatus", result[3]);
			jarray.put(jobj);
		}
		jobjmain.put("status", true);
		jobjmain.put("message",  "My feedback list");
		jobjmain.put("Data", jarray);
		return jobjmain.toString();
	}
	
	@GetMapping(value = "/getImageGallery" ,produces = { "application/json" })
	public String getImageGallery() throws Exception {
		JSONObject jobjmain = new JSONObject();
		JSONArray jarray = new JSONArray();
		List<Object[]> igList = appusersservices.getDataByFilter("SELECT * FROM imagegallary GROUP BY imageName");
		int counter=1;
		for(Object[] result : igList) {
			JSONObject jobj = new JSONObject();
			jobj.put("igId", result[0]);
			jobj.put("imgDesc", result[1].toString().trim());
			jobj.put("IGName", result[2]);
		//	jobj.put("imagePath", result[3]);
			jobj.put("imageStatus", result[4]);
			jobj.put("addedOn", formatter2.format(formatter2.parse(result[5].toString())));
			jobj.put("autoNo", counter);
			
			JSONArray jarrayinner = new JSONArray();
			List<Object[]> igSubList = appusersservices.getDataByFilter("SELECT * FROM imagegallary WHERE imageName='"+result[2].toString()+"' ");
			for(Object[] result1 : igSubList) {
				JSONObject jobjinner = new JSONObject();
				jobjinner.put("imagePath", serverURL+result1[3]);
				jarrayinner.put(jobjinner);
			}
			jobj.put("ImagesPath", jarrayinner);
			jarray.put(jobj);
			counter++;
		}
		jobjmain.put("status", true);
		jobjmain.put("message",  "Image gallery list");
		jobjmain.put("Data", jarray);
		return jobjmain.toString();
	}
	
/*	@GetMapping(value = "/getImagesPath" ,produces = { "application/json" })
	public String getImagesPath(@RequestParam("imgName") String imgName) throws Exception {
		JSONObject jobjmain = new JSONObject();
		JSONArray jarray = new JSONArray();
		List<Object[]> igList = appusersservices.getDataByFilter("SELECT * FROM imagegallary WHERE imageName='"+imgName+"' ");
		int counter=1;
		for(Object[] result : igList) {
			JSONObject jobj = new JSONObject();
			jobj.put("igId", result[0]);
			jobj.put("imgDesc", result[1].toString().trim());
			jobj.put("imageName", result[2]);
			jobj.put("imagePath", result[3]);
			jobj.put("imageStatus", result[4]);
			jobj.put("addedOn", formatter2.format(formatter2.parse(result[5].toString())));
			jobj.put("autoNo", counter);
			jarray.put(jobj);
			counter++;
		}
		jobjmain.put("status", true);
		jobjmain.put("message",  "Image Gallery Path By IG Name");
		jobjmain.put("Data", jarray);
		return jobjmain.toString();
	}*/
	
	@GetMapping(value = "/getExtraDetailsByParam" ,produces = { "application/json" })
	public String getExtraDetailsByParam(@RequestParam("pageName") String pageName) throws Exception {
		JSONObject jobjmain = new JSONObject();
		JSONArray jarray = new JSONArray();
		List<Object[]> cmpList = appusersservices.getDataByFilter("SELECT * FROM extradetails WHERE paramName='"+pageName+"' ");
		for(Object[] result : cmpList) {
			JSONObject jobj = new JSONObject();
			jobj.put("edId", result[0]);
			jobj.put("edParam", result[2].toString().trim());
			jobj.put("edDesc", result[1].toString().trim());
			jarray.put(jobj);
		}
		jobjmain.put("status", true);
		jobjmain.put("message", "Extra details list");
		jobjmain.put("Data", jarray);
		return jobjmain.toString();
	}

	
	@GetMapping(value = "/getCountryList" ,produces = { "application/json" })
	public String getCountryList() throws Exception {
		JSONObject jobjmain = new JSONObject();
		JSONArray jarray = new JSONArray();
		List<Object[]> dataList = appusersservices.getDataByFilter("SELECT * FROM country");
		for(Object[] result : dataList) {
			JSONObject jobj = new JSONObject();
			jobj.put("countryId", result[0]);
			jobj.put("countryName", result[1].toString().trim());
			if(!(Integer.parseInt(result[0].toString())==0)) {
			  jarray.put(jobj);
			}
		}
		jobjmain.put("status", true);
		jobjmain.put("message", "Country List");
		jobjmain.put("Data", jarray);
		return jobjmain.toString();
	}
	
	@GetMapping(value = "/getStateList" ,produces = { "application/json" })
	public String getStateList(@RequestParam("countryId") int countryId) throws Exception {
		JSONObject jobjmain = new JSONObject();
		JSONArray jarray = new JSONArray();
		List<Object[]> dataList = appusersservices.getDataByFilter("SELECT * FROM state WHERE countryId="+countryId+" ");
		for(Object[] result : dataList) {
			JSONObject jobj = new JSONObject();
			jobj.put("stateId", result[0]);
			jobj.put("stateName", result[1].toString().trim());
			if(!(Integer.parseInt(result[0].toString())==0)) {
			  jarray.put(jobj);
			}
		}
		jobjmain.put("status", true);
		jobjmain.put("message", "State List");
		jobjmain.put("Data", jarray);
		return jobjmain.toString();
	}
	
	@GetMapping(value = "/getCityList" ,produces = { "application/json" })
	public String getCityList(@RequestParam("stateId") int stateId) throws Exception {
		JSONObject jobjmain = new JSONObject();
		JSONArray jarray = new JSONArray();
		List<Object[]> dataList = appusersservices.getDataByFilter("SELECT * FROM city WHERE stateId="+stateId+" ");
		for(Object[] result : dataList) {
			JSONObject jobj = new JSONObject();
			jobj.put("cityId", result[0]);
			jobj.put("cityName", result[1].toString().trim());
			if(!(Integer.parseInt(result[0].toString())==0)) {
			  jarray.put(jobj);
			}
		}
		jobjmain.put("status", true);
		jobjmain.put("message", "City List");
		jobjmain.put("Data", jarray);
		return jobjmain.toString();
	}
	
	@GetMapping(value = "/getAllItemsList" ,produces = { "application/json" })
	public String getAllItemsList() throws Exception {
		JSONObject jobjmain = new JSONObject();
		JSONArray jarray = new JSONArray();
		int counter=1;
		List<Object[]> itemList = appusersservices.getDataByFilter("SELECT im.itemId,im.avalibleQty,im.itemDesc,im.itemImg1,im.itemImg2,im.itemImg3,im.itemName,im.itemPrimaryImg,im.itemStat,\r\n" + 
				"       im.marketPrice,im.maxQty,im.minQty,im.offerPrice,cm.catName,im.catId,im.itemPoints FROM itemmaster im JOIN itemcategory cm ON im.catId=cm.catId ");
		for (Object[] result : itemList) {
			JSONObject jobj = new JSONObject();
			jobj.put("itemId", result[0]);
			jobj.put("availibleQty",  result[1].toString().trim());
			jobj.put("itemDesc", result[2]);
			jobj.put("image1", result[3].toString().equals("NA") ? "NA" : serverURL+result[3]);
			jobj.put("image2", result[4].toString().equals("NA") ? "NA" : serverURL+result[4]);
			jobj.put("image3", result[5].toString().equals("NA") ? "NA" : serverURL+result[5]);
			jobj.put("itemName", result[6]);
			jobj.put("imagePrimary", result[7].toString().equals("NA") ? "NA" : serverURL+result[7]);
			jobj.put("itemStat", result[8]);
			jobj.put("marketPrice", result[9]);
			jobj.put("maxQty", result[10]);
			jobj.put("minQty", result[11]);
			jobj.put("offerPrice", result[12]);
			jobj.put("categoryName", result[13]);
			jobj.put("categoryId", result[14]);
			jobj.put("itemPoints", result[15]);
			jobj.put("autoNo", counter);
			jarray.put(jobj);
			counter++; 
		}
		jobjmain.put("status", true);
		jobjmain.put("message", "All items list");
		jobjmain.put("Data", jarray);
		return jobjmain.toString();
	}
	
}
